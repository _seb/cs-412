/**
 * @file   common.hpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Common declarations.
**/

#pragma once

// -------------------------------------------------------------------------- //
// Compiler version check

#if __cplusplus < 201103L
    #error This translation unit requires at least a C++11 compiler
#endif
#ifndef __GNUC__
    #error This translation unit requires a GNU C++ compiler
#endif

// -------------------------------------------------------------------------- //
// Macro helpers

/** Define a proposition as likely true.
 * @param prop Proposition
**/
#undef likely
#define likely(prop) \
    __builtin_expect((prop) ? 1 : 0, 1)

/** Define a proposition as likely false.
 * @param prop Proposition
**/
#undef unlikely
#define unlikely(prop) \
    __builtin_expect((prop) ? 1 : 0, 0)

// -------------------------------------------------------------------------- //
// Class helpers

/** Non copyable class.
**/
class NonCopyable {
public:
    /** Deleted copy constructor/assignment.
    **/
    NonCopyable(NonCopyable const&) = delete;
    NonCopyable& operator=(NonCopyable const&) = delete;
    /** Defaulted copy constructor/assignment.
    **/
    NonCopyable(NonCopyable&&) = default;
    NonCopyable& operator=(NonCopyable&&) = default;
    /** Default constructor.
    **/
    NonCopyable() = default;
};

/** Non copyable and movable class.
**/
class NonMoveable {
public:
    /** Defaulted copy constructor/assignment.
    **/
    NonMoveable(NonMoveable const&) = default;
    NonMoveable& operator=(NonMoveable const&) = default;
    /** Deleted copy constructor/assignment.
    **/
    NonMoveable(NonMoveable&&) = delete;
    NonMoveable& operator=(NonMoveable&&) = delete;
    /** Default constructor.
    **/
    NonMoveable() = default;
};

/** Non constructible class.
**/
class NonConstructible {
public:
    /** Deleted default constructor.
    **/
    NonConstructible() = delete;
};

/** Non instantiable class.
**/
class NonInstantiable: protected virtual NonCopyable, protected virtual NonMoveable, protected virtual NonConstructible {};
