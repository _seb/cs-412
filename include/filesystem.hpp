/**
 * @file   filesystem.hpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Low-level tailored file system management.
**/

#pragma once

// External headers
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>
extern "C" {
#include <sys/types.h>
}

// Internal headers
#include <common.hpp>
#include <constexpr.hpp>
#include <exception.hpp>
#include <flags.hpp>
#include <os.hpp>
#include <string_view.hpp>

namespace FileSystem {
// -------------------------------------------------------------------------- //
// Helper classes

/** File mode flags class alias.
**/
using FileMode = Flags::Set<class ModeId>;

/** LS-like type enum class.
**/
enum class FileType: char {
    regular   = '-',
    directory = 'd',
    symlink   = 'l',
    chardev   = 'c',
    blckdev   = 'b',
    fifopipe  = 'p',
    socket    = 's',
    unknown   = '?'
};

// -------------------------------------------------------------------------- //
// Directory walker

/** Tailored directory walker class.
**/
class Directory: public OS::Descriptor {
protected:
    /** Descriptor move constructor.
     * @param desc Directory descriptor to move
    **/
    Directory(OS::Descriptor&& desc): OS::Descriptor{::std::move(desc)} {}
public:
    Directory();
    Directory(char const*);
    Directory(::std::string const&);
    /** Explicit copy constructor.
     * @param instance Instance to copy
    **/
    explicit Directory(Directory const& instance): OS::Descriptor{OS::Descriptor{instance}} {}
    /** Deleted copy assignment.
    **/
    Directory& operator=(Directory const&) = delete;
    /** Defaulted move constructor/assignment.
    **/
    Directory(Directory&&) = default;
    Directory& operator=(Directory&&) = default;
public:
    Directory walk(char const*);
    Directory walk(::std::string const& name) {
        return walk(name.data());
    }
    Directory walk(::std::string_view const& name) {
        return walk(::std::string{name.data(), name.size()}); // Needed to have a '\0' guaranteed at the end
    }
public:
    void mkdir(char const*);
    void mkdir(::std::string const& name) {
        mkdir(name.data());
    }
    void mkdir(::std::string_view const& name) {
        mkdir(::std::string{name.data(), name.size()}); // Needed to have a '\0' guaranteed at the end
    }
public:
    void rm(char const*);
    void rm(::std::string const& name) {
        return rm(name.data());
    }
    void rm(::std::string_view const& name) {
        return rm(::std::string{name.data(), name.size()}); // Needed to have a '\0' guaranteed at the end
    }
};

// -------------------------------------------------------------------------- //
// Directory listing

/** Tailored directory listing class.
**/
class DirectoryListing: protected virtual NonCopyable {
public:
    /** Entry class.
    **/
    class Entry final {
    public:
        size_t   size; // Size of the name (not including the single-byte null-terminator)
        FileType type; // This entry type
        char   data[]; // Entry name (null-terminated)
    public:
        /** Get the entry name.
         * @return Entry name
        **/
        auto name() const noexcept {
            return ::std::string_view{data, size};
        }
        /** Get the next entry.
         * @return Next entry, may not be dereferenced
        **/
        auto next() const noexcept {
            return ConstExpr::align(this, alignof(Entry), sizeof(Entry) + size + 1); // '+1' to include the single-byte null-terminator
        }
    };
    static_assert(::std::is_standard_layout<Entry>::value, "Expected 'Entry' to be standard layout");
protected:
    /** Generating, bare minimum iterator class.
     * @param Generator Entry generator (static) class
    **/
    template<class Generator> class Iterator final {
    private:
        /** This class instance alias.
        **/
        using This = Iterator<Generator>;
    private:
        DirectoryListing const* dirfd; // Bound directory
        Entry const* limit; // Last entry
        Entry const* entry; // Current entry
    private:
        /** Advance to the next kept entry, if not already at the end.
         * @param check Whether to move to the next entry only if the current one is not kept
        **/
        void advance(bool check) {
            if (check && Generator::keep(*dirfd, *entry)) // Keep the current entry
                return;
            if (entry < limit) do {
                entry = entry->next();
            } while (entry < limit && !Generator::keep(*dirfd, *entry));
        }
    public:
        /** Bind constructor.
         * @param dirfd Bound directory
         * @param entry Initial entry
        **/
        Iterator(DirectoryListing const* dirfd, Entry const* entry, Entry const* limit): dirfd{dirfd}, limit{limit}, entry{entry} {
            advance(true);
        }
    public:
        /** (In)equality test.
         * @param instance Instance to compare with
         * @return Whether the instances are equal/different
        **/
        auto operator==(Iterator const& instance) const noexcept {
            return entry == instance.entry;
        }
        auto operator!=(Iterator const& instance) const noexcept {
            return entry != instance.entry;
        }
        /** Make the entry's associated content.
         * @return Associated content
        **/
        auto get() const {
            if (unlikely(entry >= limit))
                throw Exception::Capacity{"DirectoryListing iterator dereferencing out-of-bound"};
            return Generator::make(*dirfd, *entry);
        }
        auto operator*() const {
            return get();
        }
        /** Move to the next entry.
         * @return Current instance
        **/
        auto& next() {
            advance(false);
            return *this;
        }
        auto& operator++() {
            return next();
        }
    public:
        /** Make an iterator at the current iterator.
         * @return Begin iterator
        **/
        auto begin() const {
            return *this;
        }
        /** Make an iterator already at the end for the current iterator.
         * @return End iterator
        **/
        auto end() const {
            return This{dirfd, limit, limit};
        }
    };
protected:
    OS::FileMap entries; // Entries, one after the others
public:
    DirectoryListing();
    DirectoryListing(Directory const&);
    DirectoryListing(::std::string const&);
    DirectoryListing(char const*);
public:
    /** Make an iterator with the given generator class.
     * @param Generator Generator class
     * @return Iterator on the first kept entry
    **/
    template<class Generator> Iterator<Generator> iterate() const {
        auto&& first = ConstExpr::align(reinterpret_cast<Entry const*>(entries.get_data()));
        auto&& limit = ConstExpr::align(reinterpret_cast<Entry const*>(entries.get_data()), alignof(Entry), entries.get_size());
        return Iterator<Generator>{this, first, limit};
    }
};

namespace Generators { // --------------------------------------------------- //

/** Only sub-entry (i.e. not '.' and '..') generator wrapper static class.
 * @param Generator Wrapped generator class
**/
template<class Generator> class BelowOnly final: protected virtual NonInstantiable {
public:
    /** Ask whether to keep the entry.
     * @param dirfd Bound directory
     * @param entry Current entry
     * @return Whether to keep the entry
    **/
    static auto keep(DirectoryListing const& dirfd, DirectoryListing::Entry const& entry) {
        auto&& name = entry.name();
        return name != "." && name != ".." && Generator::keep(dirfd, entry);
    }
    /** Make a name from the given entry.
     * @param dirfd Bound directory
     * @param entry Current entry
     * @return Entry name
    **/
    static auto make(DirectoryListing const& dirfd, DirectoryListing::Entry const& entry) {
        return Generator::make(dirfd, entry);
    }
};

/** Tuple generator wrapper static class.
 * @param Generators Wrapped generator classes
**/
template<class... Generators> class Tuple final: protected virtual NonInstantiable {
public:
    /** Ask whether to keep the entry.
     * @param dirfd Bound directory
     * @param entry Current entry
     * @return Whether every wrapped generator wanted to keep the entry
    **/
    static auto keep(DirectoryListing const& dirfd, DirectoryListing::Entry const& entry) {
        return (... && Generators::keep(dirfd, entry));
    }
    /** Make a name from the given entry.
     * @param dirfd Bound directory
     * @param entry Current entry
     * @return Tuple of entry content
    **/
    static auto make(DirectoryListing const& dirfd, DirectoryListing::Entry const& entry) {
        return ::std::make_tuple(Generators::make(dirfd, entry)...);
    }
};

/** Name generator static class.
**/
class Name final: protected virtual NonInstantiable {
public:
    /** Ask whether to keep the entry.
     * @param dirfd Bound directory
     * @param entry Current entry
     * @return Whether to keep the entry
    **/
    static auto keep(DirectoryListing const&, DirectoryListing::Entry const&) {
        return true;
    }
    /** Make a name from the given entry.
     * @param dirfd Bound directory
     * @param entry Current entry
     * @return Entry name
    **/
    static auto make(DirectoryListing const&, DirectoryListing::Entry const& entry) {
        return entry.name();
    }
};

/** Is directory? generator static class.
**/
class IsDir final: protected virtual NonInstantiable {
public:
    /** Ask whether to keep the entry.
     * @param dirfd Bound directory
     * @param entry Current entry
     * @return Whether to keep the entry
    **/
    static auto keep(DirectoryListing const&, DirectoryListing::Entry const&) {
        return true;
    }
    /** Make a name from the given entry.
     * @param dirfd Bound directory
     * @param entry Current entry
     * @return Entry name
    **/
    static auto make(DirectoryListing const&, DirectoryListing::Entry const& entry) {
        return entry.type == FileType::directory;
    }
};

} // ------------------------------------------------------------------------ //

// -------------------------------------------------------------------------- //
// Read-only file mapping

/** Read-only file mapping class.
**/
class FileReadMap: public OS::FileMap {
public:
    FileReadMap(char const*);
    FileReadMap(::std::string const&);
};

// -------------------------------------------------------------------------- //
}
