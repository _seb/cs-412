/**
 * @file   flags.hpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Simple typed flag set management.
**/

#pragma once

// External headers
#include <type_traits>

// Internal headers
#include <common.hpp>

namespace Flags {
// -------------------------------------------------------------------------- //
// Base class

/** Underlying integral class alias.
**/
using Integral = uint64_t;

// -------------------------------------------------------------------------- //
// Flag set class

/** Flag set class.
 * @param id Flag identifier (incomplete) class
**/
template<class Id> class Set final {
private:
    /** This class instance alias.
    **/
    using This = Set<Id>;
private:
    /** Other classes-filtering get flags.
     * @return Raw flags value
    **/
    Integral get(Integral value) {
        return value;
    }
    Integral get(This const& set) {
        return set.flags;
    }
    template<class Any> Integral get(Set<Any> const&) = delete;
private:
    Integral flags; // Flag set
public:
    /** Typed flag set constructor.
     * @param ... Flags to merge
    **/
    template<class... Args> Set(Args&&... args) noexcept: flags{0} {
        Integral const args_flags[] = {get(args)...};
        for (auto&& flag: args_flags)
            flags |= flag;
    }
public:
    /** Check that all the given flags are present in the current instance.
     * @return Whether all the flags are present
    **/
    auto has(This const& set) const noexcept {
        return (flags & set.flags) == set.flags;
    }
};

// -------------------------------------------------------------------------- //
}
