/**
 * @file   network.hpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Low-level tailored network management.
**/

#pragma once

// External headers
#include <cstring>
#include <memory>
#include <string>
#include <type_traits>
extern "C" {
#include <netdb.h>
}

// External forward declaration
struct addrinfo;
struct sockaddr;

// Internal headers
#include <common.hpp>
#include <constexpr.hpp>
#include <exception.hpp>
#include <os.hpp>
#include <string_view.hpp>

namespace Network {
// -------------------------------------------------------------------------- //
// Helper classes

/** Address information manager class.
**/
class AddressInfo final: private virtual NonCopyable {
private:
    addrinfo* info; // Owned raw addrinfo structure
private:
    void cleanup() noexcept;
public:
    /** Move constructor/assignment.
     * @param instance Instance to move
     * @return Current instance
    **/
    AddressInfo(AddressInfo&& instance) noexcept: info{instance.info} {
        instance.info = nullptr;
    }
    AddressInfo& operator=(AddressInfo&& instance) noexcept {
        cleanup();
        info = instance.info;
        instance.info = nullptr;
        return *this;
    }
    /** Ownership-taking constructor.
     * @param info Raw pointer to take
    **/
    AddressInfo(addrinfo* info) noexcept: info{info} {}
    /** Clean-up destructor.
    **/
    ~AddressInfo() {
        cleanup();
    }
public:
    /** Access the raw structure.
     * @return Raw structure
    **/
    auto& get() const {
        if (unlikely(!info))
            throw Exception::Network{"Address information has been move away"};
        return *info;
    }
};

// -------------------------------------------------------------------------- //
// TCP-only socket mode-of-operation static classes

// Forward declaration
class Socket;

/** Connect mode-of-operation static class.
**/
class Connect final: protected virtual NonInstantiable {
public:
    static AddressInfo   make_info(char const*, char const*);
    static Socket        make_socket(addrinfo const&);
    static ::std::string make_string(addrinfo const&);
};

/** Listen mode-of-operation static class.
**/
class Listen final: protected virtual NonInstantiable {
public:
    /** TCP listening socket class.
    **/
    class ListenSocket: public OS::Descriptor {
    public:
        /** Explicit move descriptor constructor.
         * @param desc (Socket) descriptor to move
        **/
        explicit ListenSocket(OS::Descriptor&& desc) noexcept: OS::Descriptor{::std::move(desc)} {}
    public:
        /** Deleted read/write.
        **/
        size_t read(size_t, void*) = delete;
        size_t write(size_t, void const*) = delete;
        void write_all(size_t, void const*) = delete;
    public:
        Socket accept();
    };
public:
    static AddressInfo   make_info(char const*);
    static ListenSocket  make_socket(addrinfo const&);
    static ::std::string make_string(addrinfo const&);
};

// -------------------------------------------------------------------------- //
// Generic address manager class

/** Generic addresses helper class.
 * @param Mode 'Connect' or 'Listen' working mode class
**/
template<class Mode> class Addresses: protected virtual NonCopyable {
private:
    /** This class instance.
    **/
    using This = Addresses<Mode>;
protected:
    /** Single address entry, bare minimum iterator class.
    **/
    class Address final {
    private:
        addrinfo const* info; // Bound, raw information structure
    public:
        /** Bind constructor.
         * @param info Information structure to bind (optional)
        **/
        Address(addrinfo const* info = nullptr) noexcept: info{info} {}
    private:
        /** Check and get the info structure.
         * @return Valid info structure
        **/
        addrinfo const& get() const {
            if (unlikely(!info))
                throw Exception::Network{"Address is not representable"};
            return *info;
        }
    public:
        /** Inequality test.
         * @param instance Instance to compare with
         * @return Whether the instances are different
        **/
        auto operator!=(Address const& instance) const noexcept {
            return info != instance.info;
        }
        /** Dereference.
         * @return Pointed information structure
        **/
        auto& operator*() const noexcept {
            return *this;
        }
        /** Increment operation.
         * @return Current instance
        **/
        auto& operator++() noexcept {
            info = info->ai_next;
            return *this;
        }
    public:
        /** Make a new socket for that address.
         * @return New socket
        **/
        auto make_socket() const {
            return Mode::make_socket(get());
        }
        /** Generate the string representing the address.
         * @param info Given address
         * @return String representation
        **/
        ::std::string to_string() const {
            return Mode::make_string(get());
        }
    };
public:
    AddressInfo info; // Address information (may be moved away)
public:
    /** Move constructor/assignment.
     * @param instance Instance to move
     * @return Current instance
    **/
    Addresses(This&& instance) noexcept: info{::std::move(instance.info)} {}
    This& operator=(This&& instance) noexcept {
        info = ::std::move(instance.info);
        return *this;
    }
    /** Forwarding constructor.
     * @param ... Forwarded arguments
    **/
    template<class... Args> Addresses(Args&&... args): info{Mode::make_info(::std::forward<Args>(args)...)} {}
public:
    /** Begin iterator.
     * @return Begin iterator
    **/
    auto begin() const {
        return Address{&info.get()};
    }
    /** End iterator.
     * @return End iterator
    **/
    auto end()  const noexcept {
        return Address{};
    }
};

// -------------------------------------------------------------------------- //
// Higher-level socket classes

/** Simple peer name class alias.
**/
using PeerName = ::std::string;

/** TCP socket class.
**/
class Socket: public OS::Descriptor {
public:
    virtual ~Socket();
    /** Deleted copy constructor/assignment.
    **/
    Socket(Socket const&) = delete;
    Socket& operator=(Socket const&) = delete;
    /** Defaulted copy/move constructor/assignment.
    **/
    Socket(Socket&&) = default;
    Socket& operator=(Socket&&) = default;
    /** Explicit move descriptor constructor.
     * @param desc (Socket) descriptor to move
    **/
    explicit Socket(OS::Descriptor&& desc) noexcept: OS::Descriptor{::std::move(desc)} {}
    /** Host/service connection constructor.
     * @param host Target host name/IP
     * @param serv Target service name/port
    **/
    Socket(char const* host, char const* serv): Socket{[&]() {
        Network::Addresses<Network::Connect> addresses{host, serv};
        for (auto& address: addresses) {
            try {
                return address.make_socket();
            } catch (Exception::Network const& err) {}
        }
        throw Exception::Network{"Unable to connect to ", host, ":", serv};
    }()} {}
    Socket(::std::string const& host, char const* serv): Socket{host.data(), serv} {}
    Socket(char const* host, ::std::string const& serv): Socket{host, serv.data()} {}
    Socket(::std::string const& host, ::std::string const& serv): Socket{host.data(), serv.data()} {}
public:
    PeerName peer_to_string();
};

/** Multi-stack listen, TCP socket class.
**/
class Server: public OS::Multiplex<Socket> {
public:
    /** Server bind(s) and listen(s) constructor.
     * @param serv Local service name/port
    **/
    Server(char const* serv) {
        Network::Addresses<Network::Listen> addresses{serv};
        for (auto& address: addresses) {
            try {
                OS::Multiplex<Socket>::on(address.make_socket(), [&](Listen::ListenSocket& sock, OS::AbstractMultiplex::Events events) {
                    if (unlikely(!events.has(EPOLLIN))) {
                        OS::Multiplex<Socket>::forget(sock);
                        throw Exception::Network{"Listening socket ", sock.get_fd(), " is unexpectedly not readable"};
                    }
                    return sock.accept();
                });
            } catch (Exception::Network const& err) {}
        }
        if (!pullable())
            throw Exception::Network{"Unable to listen to ", serv};
    }
    Server(::std::string const& serv): Server{serv.data()} {}
public:
    /** Deleted read/write.
    **/
    size_t read(size_t, void*) = delete;
    size_t write(size_t, void const*) = delete;
    void write_all(size_t, void const*) = delete;
public:
    /** Deleted on/forget/pull/peek.
    **/
    template<class... Args> void on(Args&&...) = delete;
    template<class... Args> void forget(Args&&...) = delete;
    template<class... Args> void pull(Args&&...) = delete;
    template<class... Args> void peek(Args&&...) = delete;
public:
    /** Accept any connection.
     * @return New incoming connection
    **/
    Socket accept() {
        return OS::Multiplex<Socket>::pull();
    }
};

// -------------------------------------------------------------------------- //
}
