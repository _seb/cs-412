/**
 * @file   filequeue.hpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Tailored file queue management.
**/

#pragma once

// External headers
#include <queue>
#include <tuple>

// Internal headers
#include <common.hpp>
#include <exception.hpp>
#include <os.hpp>
#include <network.hpp>
#include <string_view.hpp>

namespace FileQueue {
// -------------------------------------------------------------------------- //
// Server-side management

/** Per-client file queue and server class.
**/
class Server final: private virtual NonCopyable, public OS::Multiplex<void> {
private:
    ::std::string port; // Port number for this client
    ::std::queue<::std::tuple<OS::Descriptor, size_t, bool>> files; // Files to transfer, queue of trio (file descriptor, size to transfer (in bytes), send?)
public:
    Server(Network::Socket const&);
public:
    ::std::string_view push(OS::Descriptor&&, size_t, bool);
};

// -------------------------------------------------------------------------- //
// Client-side management

void send(OS::Multiplex<bool>&, Network::Socket const&, uint16_t, OS::Descriptor&&, size_t);
void recv(OS::Multiplex<bool>&, Network::Socket const&, uint16_t, OS::Descriptor&&, size_t);

// -------------------------------------------------------------------------- //
}
