/**
 * @file   open.hpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Simple header-only file open helpers.
**/

#pragma once

// External headers
extern "C" {
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
}

// Internal headers
#include <common.hpp>
#include <exception.hpp>
#include <filesystem.hpp>

namespace {
// -------------------------------------------------------------------------- //
// File open helper functions

/** Convert protection flags to a string.
 * @param flags Given protection flags
 * @return Associated string
**/
inline char const* flags_to_string(int flags) noexcept {
    if (flags & O_RDWR) {
        return "read-write";
    } else if (flags & O_WRONLY) {
        return "write-only";
    } else { // Because 'O_RDONLY == 0'
        return "read-only";
    }
}

/** Open a file.
 * @param path  File path
 * @param flags Requested flags
 * @param base  Base directory (optional, current directory by default)
 * @return File descriptor
**/
inline auto openfile(char const* path, int flags) {
    auto fd = ::open(path, flags | O_CLOEXEC, 0664);
    if (unlikely(fd == -1))
        throw Exception::FileSystem{"Open file \"", path, "\" with ", flags_to_string(flags), " access error code ", errno, " (", ::std::strerror(errno), ")"};
    return OS::Descriptor{fd};
}
inline auto openfile(::std::string const& path, int flags) {
    return openfile(path.data(), flags);
}
inline auto openfile(::std::string_view const& path, int flags) {
    ::std::string copy{path.data(), path.size()};
    return openfile(copy.data(), flags);
}
inline auto openfile(char const* path, int flags, FileSystem::Directory const& base) {
    auto fd = ::openat(base.get_fd(), path, flags | O_CLOEXEC, 0664);
    if (unlikely(fd == -1))
        throw Exception::FileSystem{"Open file \"", path, "\" with ", flags_to_string(flags), " access from directory fd ", base.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
    return OS::Descriptor{fd};
}
inline auto openfile(::std::string const& path, int flags, FileSystem::Directory const& base) {
    return openfile(path.data(), flags, base);
}
inline auto openfile(::std::string_view const& path, int flags, FileSystem::Directory const& base) {
    ::std::string copy{path.data(), path.size()};
    return openfile(copy.data(), flags, base);
}

// -------------------------------------------------------------------------- //
}
