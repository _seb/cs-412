/**
 * @file   optional.hpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * C++ version-aware optional<T> include.
**/

#pragma once

// External headers
#if __cplusplus >= 201703L
    #include <optional>
#else
    #include <experimental/optional>
namespace std {
    template<class T> using optional = experimental::optional<T>;
    using experimental::nullopt;
}
#endif
