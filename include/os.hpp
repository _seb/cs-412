/**
 * @file   os.hpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Miscellaneous (descriptor, (file) mapping, etc) OS-related helpers.
**/

#pragma once

// External headers
#include <memory>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>
extern "C" {
#include <signal.h>
#include <sys/epoll.h>
#include <sys/mman.h>
#include <sys/signalfd.h>
}

// Internal headers
#include <common.hpp>
#include <constexpr.hpp>
#include <exception.hpp>
#include <flags.hpp>
#include <optional.hpp>
#include <string_view.hpp>

namespace OS {
// -------------------------------------------------------------------------- //
// Persistent 'sysconf'

/** Supported sysconf names enum class.
**/
enum class SysConf: size_t { // Order and numbering matter (internally used as indexes)
    pagesize = 0,
    nbcores  = 1
};

long sysconf(SysConf);

// -------------------------------------------------------------------------- //
// Base descriptor

/** Simple descriptor class.
**/
class Descriptor {
public:
    /** Raw descriptor class alias.
    **/
    using Fd = int;
public:
    constexpr static Fd   invalid = -1;   // Invalid file descriptor
    constexpr static bool observe = true; // To make the copy-constructors more explicit
protected:
    Fd   fd;  // File descriptor
    bool obs; // Whether the descriptor is not closed on destruction (ignored if 'fd' is invalid)
private:
    static Fd dup(Fd);
public:
    /** Explicit copy constructor.
     * @param instance Instance to copy
    **/
    explicit Descriptor(Descriptor const& instance): fd{instance.obs ? instance.fd : dup(instance.fd)}, obs{instance.obs} {}
    /** Deleted copy assignment.
    **/
    Descriptor& operator=(Descriptor const&) = delete;
    /** Move constructor/assignment.
     * @param instance Instance to move
     * @return Current instance
    **/
    Descriptor(Descriptor&& instance) noexcept: fd{instance.fd}, obs{instance.obs} {
        if (!obs)
            instance.fd = invalid;
    }
    Descriptor& operator=(Descriptor&& instance) noexcept {
        close();
        fd  = instance.fd;
        obs = instance.obs;
        if (!obs)
            instance.fd = invalid;
        return *this;
    }
    /** Explicit copy/raw/empty constructor.
     * @param fd  File descriptor to manage (optional)
     * @param obs Whether 'fd' is observed (not closed on destruction)
    **/
    Descriptor(Fd fd = invalid, bool obs = false) noexcept: fd{fd}, obs{obs} {}
    explicit Descriptor(Descriptor const& instance, bool obs): fd{obs ? instance.fd : dup(instance.fd)}, obs{obs} {}
    /** Close destructor.
    **/
    virtual ~Descriptor() {
        close();
    }
public:
    /** Check whether the two underlying descriptor are identical/different.
     * @param instance Instance to compare with
     * @return Whether the two underlying descriptor are identical/different
    **/
    bool operator==(Descriptor const& instance) const noexcept {
        return instance.fd == fd;
    }
    bool operator!=(Descriptor const& instance) const noexcept {
        return instance.fd != fd;
    }
public:
    /** Get the raw file descriptor.
     * @return File descriptor
    **/
    auto get_fd() const noexcept {
        return fd;
    }
    /** Check whether the file descriptor is not invalid.
     * @return File descriptor is not marked invalid
    **/
    auto is_valid_fd() const noexcept {
        return fd != invalid;
    }
public:
    void close() noexcept;
    size_t read(size_t, void*);
    size_t write(size_t, void const*);
    void write_all(size_t, void const*);
};

// FIXME: Would be cleaner to go back to virtual inheritance for Descriptor, and two {Read,Write}ableDescriptor derived classes?

// -------------------------------------------------------------------------- //
// Descriptor multiplexing management

/** Pseudo-abstract multiplexing class.
**/
class AbstractMultiplex: protected virtual NonCopyable, public Descriptor {
public:
    /** Single event identifier flag class.
    **/
    using Event = int;
    using Events = Flags::Set<class EventsId>;
protected:
    /** Abstract callback class.
    **/
    class AbstractCallback: protected virtual NonCopyable, protected virtual NonMoveable {
    public:
        /** No-op destructor.
        **/
        virtual ~AbstractCallback() {}
    public:
        virtual void call(Events, void*) = 0;
    };
    /** Map descriptor to callbacks class alias.
    **/
    using Callbacks  = ::std::unordered_map<OS::Descriptor::Fd, ::std::unique_ptr<AbstractCallback>>;
    /** Callbacks which deletion has been defered class alias.
    **/
    using Defered = ::std::vector<::std::unique_ptr<AbstractCallback>>;
protected:
    Callbacks callbacks; // Registered callbacks
    Defered   defered;   // Defered-delete callbacks
    bool  defer = false; // Whether to defer callback deletion (to support callbacks replacing/erasing themselves)
private:
    static Descriptor make_fd();
    void set(Descriptor const&, Event, AbstractCallback*, bool);
    void reset(Descriptor const&);
protected:
    /** Make descriptor constructor.
    **/
    AbstractMultiplex(): Descriptor{make_fd()} {}
protected:
    ::std::pair<AbstractCallback*, Events> next(bool);
    /** Register/replace a lambda callback on descriptor events; basic exception guarantee.
     * @param desc   Descriptor to watch
     * @param val    Callback to use
     * @param events Event to trigger on (optional)
    **/
    void on(Descriptor const& desc, ::std::unique_ptr<AbstractCallback>&& val, Event events = EPOLLIN) {
        auto&& key  = desc.get_fd();
        auto entry  = callbacks.find(key);
        auto exists = entry != callbacks.end();
        set(desc, events, val.get(), exists);
        try {
            if (exists) { // Replace
                if (defer)
                    defered.emplace_back(::std::move(entry->second));
                entry->second = ::std::move(val);
            } else { // Create
                callbacks.emplace(key, ::std::move(val));
            }
        } catch (...) {
            reset(desc);
            throw;
        }
    }
public:
    /** Unregister the associated callback of a descriptor, ignore if inexistent; basic exception guarantee.
     * @param desc Descriptor to unwatch, should be an observer
    **/
    void forget(Descriptor const& desc) {
        auto&& entry = callbacks.find(desc.get_fd());
        // Check if exists
        if (entry == callbacks.end())
            return;
        // Remove entry
        reset(Descriptor{entry->first, Descriptor::observe});
        if (defer)
            defered.emplace_back(::std::move(entry->second));
        callbacks.erase(entry);
    }
    /** Unregister all the associated callbacks; basic exception guarantee.
    **/
    void clear() {
        while (callbacks.size() > 0) {
            auto entry = callbacks.begin();
            reset(Descriptor{entry->first, Descriptor::observe});
            if (defer)
                defered.emplace_back(::std::move(entry->second));
            callbacks.erase(entry);
        }
    }
    /** Return whether there is at least one event registered.
     * @return Whether there is at least one event registered
    **/
    auto pullable() const noexcept {
        return callbacks.size() > 0;
    }
};

/** Multiplexing class.
 * @param Type Returned type
**/
template<class Type> class Multiplex: public AbstractMultiplex {
    static_assert(::std::is_same<Type, typename ::std::remove_reference<Type>::type>::value, "Expected non-reference class 'Type'");
private:
    /** Lambda callback class.
     * @param Desc   Descriptor class
     * @param Lambda Callable class
    **/
    template<class Desc, class Lambda> class Callback final: public AbstractCallback {
        static_assert(::std::is_base_of<Descriptor, Desc>::value, "Expected 'Desc' to derive from 'Descriptor'");
    private:
        Desc   desc;   // Stored (derived) descriptor
        Lambda lambda; // Stored lambda callback
    public:
        /** Register constructor.
         * @param desc   Descriptor to move
         * @param lambda Lambda to store
        **/
        Callback(Desc&& desc, Lambda&& lambda): desc{::std::move(desc)}, lambda{::std::move(lambda)} {}
    public:
        /** Call the lambda, store the result in the given storage.
         * @param events  Event flag set to pass to the lambda
         * @param storage Output storage of the return type
        **/
        void call(Events events, void* storage) override {
            new(storage) Type{lambda(desc, events)};
        }
    };
public:
    /** Register/replace a lambda callback on descriptor events.
     * @param desc   Descriptor to watch
     * @param lambda Lambda callback to use
     * @param events Event to trigger on (optional)
    **/
    template<class Desc, class Lambda> void on(Desc&& desc, Lambda&& lambda, Event events = EPOLLIN) {
        Descriptor obs{desc, Descriptor::observe};
        AbstractMultiplex::on(obs, ::std::make_unique<Callback<Desc, Lambda>>(::std::move(desc), ::std::move(lambda)), events);
    }
private:
    /** Processing one event, wait if none ready.
     * @param peek Whether return immediately if no event is ready (optional)
     * @return Forwarded return value, if any
    **/
    ::std::optional<Type> process(bool peek) {
        // Get one ready (abstract) callback, if any
        AbstractCallback* acb;
        Events events;
        ::std::tie(acb, events) = next(peek);
        if (!acb)
            return ::std::nullopt;
        // Prepare storage, call (with defered callback deletion) and return move-built optional
        typename ::std::aligned_storage<sizeof(Type), alignof(Type)>::type storage;
        defer = true;
        try {
            acb->call(events, &storage);
            defer = false;
            defered.clear();
        } catch (...) {
            defer = false;
            defered.clear();
            throw;
        }
        try {
            return ::std::move(*reinterpret_cast<Type*>(&storage)); // Statement may throw iff 'Type(Type&&)' may throw
        } catch (...) {
            reinterpret_cast<Type*>(&storage)->~Type(); // Since the instance is not managed by RAII here
            throw;
        }
    }
public:
    /** Processing one event, wait if none ready.
     * @return Forwarded return value
    **/
    Type pull() {
        // Check if not empty
        if (unlikely(!pullable()))
            throw Exception::OS{"Multiplex ", get_fd(), " pull empty callback set"}; // Throw instead of waiting indefinitely
        // Process
        auto opt = process(false);
        if (unlikely(!opt))
            throw Exception::OS{"Multiplex ", get_fd(), " pulled no value"};
        return ::std::move(*opt);
    }
    /** Processing one event, if ready.
     * @return Forwarded return value, if any
    **/
    ::std::optional<Type> peek() {
        return process(true);
    }
};
template<> class Multiplex<void>: public AbstractMultiplex {
private:
    /** Lambda callback class.
     * @param Desc   Descriptor class
     * @param Lambda Callable class
    **/
    template<class Desc, class Lambda> class Callback final: public AbstractCallback {
        static_assert(::std::is_base_of<Descriptor, Desc>::value, "Expected 'Desc' to derive from 'Descriptor'");
    private:
        Desc   desc;   // Stored (derived) descriptor
        Lambda lambda; // Stored lambda callback
    public:
        /** Register constructor.
         * @param desc   Descriptor to move
         * @param lambda Lambda to store
        **/
        Callback(Desc&& desc, Lambda&& lambda): desc{::std::move(desc)}, lambda{::std::move(lambda)} {}
    public:
        /** Call the lambda.
         * @param events  Event flag set to pass to the lambda
         * @param storage <Ignored>
        **/
        void call(Events events, void*) override {
            lambda(desc, events);
        }
    };
public:
    /** Register/replace a lambda callback on descriptor events.
     * @param desc   Descriptor to watch
     * @param lambda Lambda callback to use
     * @param events Event to trigger on (optional)
    **/
    template<class Desc, class Lambda> void on(Desc&& desc, Lambda&& lambda, Event events = EPOLLIN) {
        Descriptor obs{desc, Descriptor::observe};
        AbstractMultiplex::on(obs, ::std::make_unique<Callback<Desc, Lambda>>(::std::move(desc), ::std::move(lambda)), events);
    }
private:
    /** Processing one event, wait if none ready.
     * @param peek Whether return immediately if no event is ready (optional)
     * @return Whether a callback has been called
    **/
    bool process(bool peek) {
        // Get one ready (abstract) callback, if any
        AbstractCallback* acb;
        Events events;
        ::std::tie(acb, events) = next(peek);
        if (!acb)
            return false;
        // Call (with defered callback deletion)
        defer = true;
        try {
            acb->call(events, nullptr);
            defer = false;
            defered.clear();
        } catch (...) {
            defer = false;
            defered.clear();
            throw;
        }
        return true;
    }
public:
    /** Processing one event, wait if none ready.
    **/
    void pull() {
        // Check if not empty
        if (unlikely(!pullable()))
            throw Exception::OS{"Multiplex ", get_fd(), " pull empty callback set"}; // Throw instead of waiting indefinitely
        // Process
        process(false);
    }
    /** Processing one event, if ready.
     * @return Whether a callback has been called
    **/
    auto peek() {
        return process(true);
    }
};

// -------------------------------------------------------------------------- //
// Event management

/** Poll-able raise-once event class.
**/
class Oneshot: public OS::Descriptor {
public:
    Oneshot();
public:
    void raise();
};

// -------------------------------------------------------------------------- //
// Signal management

namespace Signal { // ------------------------------------------------------- //

/** Signal identifier class alias.
**/
using Id = int;

/** Mask class alias.
**/
using Mask = ::sigset_t;

/** Make signal mask.
 * @param ... Signal identifiers to mask
 * @return Associated signal mask
**/
template<class... Args> static auto make_mask(Args&&... args) {
    Id ids[] = {::std::forward<Args>(args)...};
    Mask do_make_mask(size_t, Id const*);
    return do_make_mask(sizeof...(args), ids);
}

} // ------------------------------------------------------------------------ //

/** Action associated with a context enum class.
**/
enum class SignalAction: int {
    block = SIG_BLOCK,
    unblock = SIG_UNBLOCK,
    set = SIG_SETMASK
};

/** Signal context block/unblock/set class.
**/
class SignalContext: protected virtual NonCopyable, protected virtual NonMoveable { // Safeguard as use is only "position-based" in the code, allowing moves could induce reordering bugs
private:
    static void apply_mask(SignalAction, Signal::Mask const*, Signal::Mask*);
protected:
    Signal::Mask oldmask; // Mask before this context
    Signal::Mask newmask; // Mask before this context
public:
    virtual ~SignalContext();
    /** Signal context enter constructor.
     * @param ... Signal identifiers to catch
    **/
    template<class... Args> SignalContext(SignalAction action, Args&&... args): newmask{Signal::make_mask(::std::forward<Args>(args)...)} {
        apply_mask(action, &newmask, &oldmask);
    }
};

/** Signal queue descriptor class.
**/
class SignalQueue: public Descriptor {
public:
    /** Signal info class alias.
    **/
    using Entry = ::signalfd_siginfo;
private:
    static Descriptor::Fd make_fd(Signal::Mask const&);
public:
    /** Queue constructor.
     * @param ... Signal identifiers to catch
    **/
    template<class... Args> SignalQueue(Args&&... args): Descriptor{make_fd(Signal::make_mask(::std::forward<Args>(args)...))} {}
public:
    Entry pop();
};

// -------------------------------------------------------------------------- //
// (File) mapping

/** (File) mapping class.
**/
class FileMap: protected virtual NonCopyable {
public:
    /** Protection enum class.
    **/
    enum class Protection: int {
        ___ = PROT_NONE,
        r__ = PROT_READ,
        _w_ = PROT_WRITE,
        __x = PROT_EXEC,
        rw_ = PROT_READ | PROT_WRITE,
        r_x = PROT_READ | PROT_EXEC,
        _wx = PROT_WRITE | PROT_EXEC,
        rwx = PROT_READ | PROT_WRITE | PROT_EXEC
    };
protected:
    void*       data; // First page pointer (nullptr when unallocated)
    size_t used_size; // Declared allocation size (0 when unallocated)
    size_t virt_size; // Reserved allocation in virtual memory (0 when unallocated)
    Protection  prot; // Associated protection
protected:
    void map(Descriptor const&, size_t, bool);
    bool remap(size_t, bool, bool);
    void unmap() noexcept;
public:
    /** Move constructor/assignment.
     * @param instance Instance to move
     * @return Current instance
    **/
    FileMap(FileMap&& instance) noexcept: data{::std::move(instance.data)}, used_size{::std::move(instance.used_size)}, virt_size{::std::move(instance.virt_size)} {
        instance.data = nullptr;
    }
    FileMap& operator=(FileMap&& instance) noexcept {
        unmap();
        data      = ::std::move(instance.data);
        prot      = ::std::move(instance.prot);
        used_size = ::std::move(instance.used_size);
        virt_size = ::std::move(instance.virt_size);
        instance.data = nullptr;
        return *this;
    }
    /** Mapping constructor.
     * @param size   Size to allocate (in bytes), must not be 0
     * @param prot   Protection to use (optional)
     * @param desc   Descriptor to use (optional)
     * @param shared Whether the mapping is shared (optional, ignored if size is null)
    **/
    FileMap(size_t size = 0, Protection prot = Protection::rw_, Descriptor const& desc = Descriptor{}, bool shared = false): data{nullptr}, used_size{0}, virt_size{0}, prot{prot} {
        map(desc, size, shared);
    }
    /** Mapping destructor.
    **/
    virtual ~FileMap() {
        unmap();
    }
public:
    /** Get a raw data pointer.
     * @return Raw data pointer, nullptr for none
    **/
    auto get_data() const noexcept {
        return data;
    }
    /** Get a declared size.
     * @return Declared size
    **/
    auto get_size() const noexcept {
        return used_size;
    }
public:
    /** Resize the mapping, may invalidate pointers.
     * @param size  New size (in bytes)
     * @param fixed Never invalidate pointers
     * @param force Always perform the remapping (even when shrinking)
     * @return Whether any pointer on the mapping is now invalid
    **/
    bool resize(size_t size, bool fixed = false, bool force = false) {
        return data ? remap(size, fixed, force) : (map(Descriptor{}, size, false), true);
    }
};

char const* to_string(FileMap::Protection);

// -------------------------------------------------------------------------- //
}
