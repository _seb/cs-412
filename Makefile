# Configuration
MKFILE   := $(lastword $(MAKEFILE_LIST))
CXX      := c++
CXXFLAGS := -Wall -Wextra -Wfatal-errors -Iinclude -O3 -std=c++17
LD       := c++
LDFLAGS  :=
LDLIBS   := -lpthread

# Binary-specific additional configurations
LDLIBS_client :=
LDLIBS_server :=

# Common objects (auto-generated)
CHDR := $(wildcard include/*.hpp)
CSRC := $(wildcard src/*.cpp)
COBJ := $(CSRC:%=%.o)

# Specific objects (auto-generated)
SOBJ :=
BINS :=

.PHONY: default build clean

default: build

define MAKE_OBJECT =
$(1).o: $(1) $(2) $$(CHDR) $$(MKFILE)
	$$(CXX) $$(CXXFLAGS) -c -o $(1).o $(1)
endef

define MAKE_BINARY =
$(foreach SRC,$(3),$(eval $(call MAKE_OBJECT,$(SRC),$(4))))
bin/$(1): $(2) $$(COBJ) $$(MKFILE)
	$$(LD) $$(LDFLAGS) $$(LDFLAGS_$(1)) -o bin/$(1) $(2) $$(COBJ) $$(LDLIBS) $$(LDLIBS_$(1))
SOBJ += $(2) bin/$(1)
BINS += bin/$(1)
endef

REGISTER_BINARY = $(eval $(call MAKE_BINARY,$(1),$(foreach SRC,$(2),$(SRC).o),$(2),$(3)))
WILDCARD_BINARY = $(call REGISTER_BINARY,$(1),$(wildcard src/$(1)/*.cpp),$(wildcard src/$(1)/*.hpp))
$(foreach DIR,$(filter %/,$(wildcard src/*/)),$(call WILDCARD_BINARY,$(notdir $(patsubst %/,%,$(DIR)))))

$(foreach SRC,$(wildcard src/*.cpp),$(eval $(call MAKE_OBJECT,$(SRC),)))

build: $(COBJ) $(BINS)
clean:
	$(RM) $(COBJ) $(SOBJ)
