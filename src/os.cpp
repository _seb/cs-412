/**
 * @file   os.cpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Miscellaneous (descriptor, (file) mapping, etc) OS-related helpers.
**/

// External headers
#include <cerrno>
#include <cstring>
#include <iostream>
extern "C" {
#include <fcntl.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/mman.h>
#include <unistd.h>
}

// Internal headers
#include <common.hpp>
#include <constexpr.hpp>
#include <exception.hpp>
#include <os.hpp>

namespace OS {
// -------------------------------------------------------------------------- //
// Persistent 'sysconf'

/** Get, keep and return 'sysconf' for the requested name.
 * @param name Requested name
 * @return Associated value
**/
long sysconf(SysConf name) {
    /** Simple cached 'sysconf' entry.
    **/
    class Entry final {
    private:
        char const* name; // Associated name
        long value; // Value returned by 'sysconf'
        int  error; // Value of 'errno' (0 for success)
    public:
        /** Query constructor.
         * @param query Query 'sysconf' with
         * @param name  Associated preprocessor name
        **/
        Entry(int query, char const* name) noexcept: name{name} {
            errno = 0;
            value = ::sysconf(query);
            error = errno;
        }
    public:
        /** Get the cached entry.
         * @return Cached entry
        **/
        auto get() const {
            if (unlikely(error != 0))
                throw Exception::OS{"'sysconf(", name, ")' failed with error code ", error, " (", ::std::strerror(error), ")"};
            return value;
        }
    };
#define MAKE_ENTRY(NAME) {_SC_##NAME, #NAME}
    static Entry entries[] = {
        MAKE_ENTRY(PAGESIZE),
        MAKE_ENTRY(NPROCESSORS_ONLN)
    }; // The order must match that of enum class 'SysConf'
#undef MAKE_ENTRY
    // Query
    auto index = static_cast<size_t>(name);
    if (unlikely(index >= sizeof(entries) / sizeof(*entries)))
        throw Exception::Unreachable{"Queried name is not a registered member of 'SysConf' enum class"};
    return entries[index].get();
}

// -------------------------------------------------------------------------- //
// Base descriptor

/** Close the managed file descriptor, if any.
**/
void Descriptor::close() noexcept {
    if (fd != invalid && !obs) {
        ::close(fd); // Ignore failure
        fd = invalid;
    }
}

/** Duplicate the given file descriptor.
 * @param fd Given file descriptor
 * @return Duplicated file descriptor
**/
Descriptor::Fd Descriptor::dup(Fd fd) {
    auto res = ::fcntl(fd, F_DUPFD_CLOEXEC);
    if (unlikely(res == -1))
        throw Exception::OS{"Duplication of ", fd, " error code ", errno, " (", ::std::strerror(errno), ")"};
    return res;
}

/** Read from the file descriptor.
 * @param len Size (in bytes) of the buffer
 * @param buf Pointer to the buffer
 * @return Size (in bytes) actually read
**/
size_t Descriptor::read(size_t len, void* buf) {
    auto res = ::read(fd, buf, len);
    if (unlikely(res < 0))
        throw Exception::OS{"Read ", len, " bytes from ", fd, " error code ", errno, " (", ::std::strerror(errno), ")"};
    return res;
}

/** Write to the file descriptor.
 * @param len Size (in bytes) of the buffer
 * @param buf Pointer to the buffer
 * @return Size (in bytes) actually written
**/
size_t Descriptor::write(size_t len, void const* buf) {
    auto res = ::write(fd, buf, len);
    if (unlikely(res < 0))
        throw Exception::OS{"Write ", len, " bytes to ", fd, " error code ", errno, " (", ::std::strerror(errno), ")"};
    return res;
}

/** Write to the file descriptor the whole given buffer.
 * @param len Size (in bytes) of the buffer
 * @param buf Pointer to the buffer
**/
void Descriptor::write_all(size_t len, void const* buf) {
    size_t written = 0;
    while (written < len)
        written += write(len - written, reinterpret_cast<void const*>(reinterpret_cast<uintptr_t>(buf) + written));
}

// -------------------------------------------------------------------------- //
// Descriptor multiplexing management

/** Make a new multiplex instance.
 * @return Associated descriptor
**/
Descriptor AbstractMultiplex::make_fd() {
    auto res = ::epoll_create1(EPOLL_CLOEXEC);
    if (unlikely(res == -1))
        throw Exception::OS{"Multiplex make error code ", errno, " (", ::std::strerror(errno), ")"};
    return res;
}

/** Register a callback for given events on the given descriptor, update it if exists.
 * @param desc   Given descriptor
 * @param events Given events
 * @param cb     Callback to call
 * @param modif  Whether to modify an existing entry
**/
void AbstractMultiplex::set(Descriptor const& desc, Event events, AbstractCallback* cb, bool modif) {
    ::epoll_event epev;
    epev.events = events;
    epev.data.ptr = cb;
    if (unlikely(::epoll_ctl(get_fd(), modif ? EPOLL_CTL_MOD : EPOLL_CTL_ADD, desc.get_fd(), &epev) != 0))
        throw Exception::OS{"Multiplex ", get_fd(), " set ", desc.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
}

/** Unregister the callback for a given descriptor.
 * @param desc Given descriptor, should be an observer
**/
void AbstractMultiplex::reset(Descriptor const& desc) {
    if (unlikely(::epoll_ctl(get_fd(), EPOLL_CTL_DEL, desc.get_fd(), nullptr) != 0))
        throw Exception::OS{"Multiplex ", get_fd(), " reset ", desc.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
}

/** Peek/wait for one event.
 * @param peek Whether never to wait
 * @return Any ready (abstract) callback (nullptr for none), associated event (undefined if no callback)
**/
::std::pair<AbstractMultiplex::AbstractCallback*, AbstractMultiplex::Events> AbstractMultiplex::next(bool peek) {
    ::epoll_event events; // One at a time to being able to forward exceptions
    auto length = ::epoll_wait(get_fd(), &events, 1, peek ? 0 : -1);
    if (unlikely(length < 0)) {
        throw Exception::OS{"Multiplex ", get_fd(), " wait error code ", errno, " (", ::std::strerror(errno), ")"};
    } else if (length == 0) { // No more event ready to process
        return ::std::pair<AbstractMultiplex::AbstractCallback*, AbstractMultiplex::Events>{nullptr, Events{}};
    } else {
        return ::std::pair<AbstractMultiplex::AbstractCallback*, AbstractMultiplex::Events>{reinterpret_cast<AbstractCallback*>(events.data.ptr), static_cast<Event>(events.events)};
    }
}

// -------------------------------------------------------------------------- //
// Event management

/** Raise-once event constructor.
**/
Oneshot::Oneshot(): Descriptor{[&]() {
    auto res = ::eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK);
    if (unlikely(res == -1))
        throw Exception::OS{"Event descriptor make error code ", errno, " (", ::std::strerror(errno), ")"};
    return res;
}()} {}

/** Raise the event, undefined behavior if called more than 2^64 - 2 times.
**/
void Oneshot::raise() {
    uint64_t value = 1;
    write(sizeof(value), &value);
}

// -------------------------------------------------------------------------- //
// Signal management

namespace Signal { // ------------------------------------------------------- //

/** Make a mask from a given array of signal identifers.
 * @param length Number of signal identifiers
 * @param ids    Signal identifiers
 * @return Signal mask
**/
Mask do_make_mask(size_t length, Id const* ids) {
    Mask mask;
    ::sigemptyset(&mask);
    for (size_t i = 0; i < length; ++i) {
        if (unlikely(::sigaddset(&mask, ids[i]) != 0))
            throw Exception::OS{"Signal add ", ids[i], " to mask error code ", errno, " (", ::std::strerror(errno), ")"};
    }
    return mask;
}

} // ------------------------------------------------------------------------ //

/** Apply a given mask.
 * @param action  Action to take
 * @param newmask Pointer to the new mask to apply
 * @param oldmask Pointer to the mask to set with the current mask (optional)
**/
void SignalContext::apply_mask(SignalAction action, Signal::Mask const* newmask, Signal::Mask* oldmask) {
    if (unlikely(::sigprocmask(static_cast<int>(action), newmask, oldmask) != 0))
        throw Exception::OS{"Signal mask apply error code ", errno, " (", ::std::strerror(errno), ")"};
}

/** Signal context leave destructor.
**/
SignalContext::~SignalContext() {
    try {
        apply_mask(SignalAction::set, &oldmask, nullptr);
    } catch (::std::exception const& err) { // To make destructor 'noexcept' in practice
        ::std::cerr << "[~SignalContext] " << err.what() << ::std::endl;
    }
}

/** Make a new signal file descriptor for the given mask.
 * @param mask Given mask
 * @return Signal file descriptor
**/
Descriptor::Fd SignalQueue::make_fd(Signal::Mask const& mask) {
    auto res = ::signalfd(-1, &mask, SFD_CLOEXEC);
    if (unlikely(res == -1))
        throw Exception::OS{"Signal descriptor make error code ", errno, " (", ::std::strerror(errno), ")"};
    return res;
}

/** Pop (wait for and read) one entry.
 * @return Signal entry
**/
SignalQueue::Entry SignalQueue::pop() {
    Entry res;
    if (unlikely(read(sizeof(res), &res) != sizeof(res)))
        throw Exception::OS{"Signal descriptor ", get_fd(), " read error code ", errno, " (", ::std::strerror(errno), ")"};
    return res;
}

// -------------------------------------------------------------------------- //
// (File) mapping

/** Return the protection string associated with the given protection.
 * @param prot Given protection
 * @return Associated protection string
**/
char const* to_string(FileMap::Protection prot) {
    switch (prot) {
    case FileMap::Protection::___:
        return "without any";
    case FileMap::Protection::r__:
        return "with read-only";
    case FileMap::Protection::_w_:
        return "with write-only";
    case FileMap::Protection::__x:
        return "with execute-only";
    case FileMap::Protection::rw_:
        return "with read-write";
    case FileMap::Protection::r_x:
        return "with read-execute";
    case FileMap::Protection::_wx:
        return "with write-execute";
    case FileMap::Protection::rwx:
        return "with full";
    default:
        throw Exception::Unreachable{"Queried protection is not a member of 'Protection' enum class"};
    };
}

/** Make the mapping, none must exist.
 * @param desc   Descriptor to map (may be invalid for anonymous mapping)
 * @param size   Minimal size to allocate (in bytes), must not be 0
 * @param shared Whether the mapping is shared
**/
void FileMap::map(Descriptor const& desc, size_t size, bool shared) {
    auto const anon = !desc.is_valid_fd();
    // Assertion on size
    if (unlikely(size == 0)) {
        if (anon) {
            throw Exception::UserFault{"Anonymous map of size 0 is forbidden"};
        } else {
            throw Exception::UserFault{"File ", desc.get_fd(), " map of size 0 is forbidden"};
        }
    }
    // Make flags
    auto flags = (shared ? MAP_SHARED : MAP_PRIVATE);
    if (anon)
        flags |= MAP_ANONYMOUS;
    // Mapping
    auto newdata = ::mmap(nullptr, size, static_cast<int>(prot), flags, desc.get_fd(), 0); // Silently allocates data even if null-sized
    if (unlikely(newdata == MAP_FAILED)) {
        if (anon) {
            throw Exception::OS{"Anonymous map ", size, " bytes ", to_string(prot), " access error code ", errno, " (", ::std::strerror(errno), ")"};
        } else {
            throw Exception::OS{"File ", desc.get_fd(), " map ", size, " bytes ", to_string(prot), " access error code ", errno, " (", ::std::strerror(errno), ")"};
        }
    }
    // Assignment
    data = newdata;
    used_size = size;
    virt_size = ConstExpr::align(size, sysconf(SysConf::pagesize));
}

/** Resize the mapping, may invalidate pointers.
 * @param size  New size (in bytes), must not be 0
 * @param fixed Never invalidate pointers
 * @param force Always perform the remapping (even when shrinking)
 * @return Whether any pointer on the mapping is now invalid
**/
bool FileMap::remap(size_t size, bool fixed, bool force) {
    // Assertion on size
    if (unlikely(size == 0))
        throw Exception::UserFault{"Remap to size 0 is forbidden"};
    // Possible quick path
    if (!force && size < virt_size) {
        used_size = size;
        return false;
    }
    // Actual remap
    auto newdata = ::mremap(data, used_size, size, fixed ? MREMAP_FIXED : MREMAP_MAYMOVE);
    if (unlikely(newdata == MAP_FAILED))
        throw Exception::OS{"Remap from ", used_size, " to ", size, " bytes ", to_string(prot), " access error code ", errno, " (", ::std::strerror(errno), ")"};
    auto res = newdata != data;
    data = newdata;
    used_size = size;
    virt_size = ConstExpr::align(size, sysconf(SysConf::pagesize));
    return res;
}

/** Remove the mapping, if any.
**/
void FileMap::unmap() noexcept {
    if (data) {
        ::msync(data, used_size, MS_ASYNC); // Out of completeness
        ::munmap(data, virt_size);
        data = nullptr;
        used_size = 0;
        virt_size = 0;
    }
}

// -------------------------------------------------------------------------- //
}
