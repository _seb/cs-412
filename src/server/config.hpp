/**
 * @file   error.hpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Forwarded server error class.
**/

#pragma once

// External headers
#include <exception>
#include <string>
#include <unordered_map>

// Internal headers
#include <common.hpp>
#include <exception.hpp>
#include <filesystem.hpp>

namespace Configuration {
// -------------------------------------------------------------------------- //
// Configuration parser and storage

class Configuration final {
private:
    /** Map username -> password class alias.
    **/
    using Users = ::std::unordered_map<::std::string, ::std::string>;
private:
    Users                users; // User list
    FileSystem::Directory root; // Root directory
    ::std::string      service; // Port name/number
public:
    Configuration(FileSystem::FileReadMap const&);
public:
    /** Get the user map.
     * @return User map
    **/
    auto const& get_users() const noexcept {
        return users;
    }
    /** Get the root directory.
     * @return Root directory
    **/
    auto const& get_root() const noexcept {
        return root;
    }
    /** Get the port name/number.
     * @return Port name/number
    **/
    auto const& get_service() const noexcept {
        return service;
    }
};

// -------------------------------------------------------------------------- //
}
