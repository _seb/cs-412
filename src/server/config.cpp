/**
 * @file   config.cpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Configuration reader.
**/

// External headers
#include <iostream>

// Internal headers
#include <common.hpp>
#include <constexpr.hpp>
#include <exception.hpp>
#include "config.hpp"

namespace Configuration {
// -------------------------------------------------------------------------- //
// Configuration parse and store

/** Parser constructor.
 * @param config Configuration file
**/
Configuration::Configuration(FileSystem::FileReadMap const& config) {
    /** Parser state machine class.
    **/
    class Parser final {
    private:
        /** Token class alias.
        **/
        using Token = ::std::string_view;
        /** Digestion context.
        **/
        enum class Context {
            none, // No context
            base, // Waiting for base directory path
            port, // Waiting for port name/number
            user, // Waiting for username
            pass  // Waiting for password
        };
    private:
        Users&                users; // Bound 'users'
        FileSystem::Directory& root; // Bound 'root'
        ::std::string&      service; // Bound 'service'
        Context             context; // Current digestion context
        ::std::string_view username; // Contextual user name (when waiting password)
        bool            custom_root; // Whether a custom root was specified
    public:
        /** Bind constructor.
         * @param config Configuration to build
        **/
        Parser(Configuration& config) noexcept: users{config.users}, root{config.root}, service{config.service}, context{Context::none}, username{}, custom_root{false} {}
    public:
        /** Digest a token.
         * @param token Token to digest (empty for line-break)
         * @return Whether the next token should be the remaining of the current line
        **/
        bool digest(Token const& token) {
            switch (context) {
            case Context::base:
                if (unlikely(token.empty()))
                    throw Exception::UserFault{"Unexpected end-of-line while parsing the base directory path in the configuration file"};
                if (unlikely(custom_root))
                    throw Exception::UserFault{"Base directory specified more than once in the configuration file"};
                root = FileSystem::Directory{::std::string{token}};
                custom_root = true;
                context = Context::none;
                return false;
            case Context::port:
                if (unlikely(token.empty()))
                    throw Exception::UserFault{"Unexpected end-of-line while parsing the port name/number in the configuration file"};
                if (unlikely(service.size() > 0))
                    throw Exception::UserFault{"Port name/number specified more than once in the configuration file"};
                service = ::std::string{token};
                context = Context::none;
                return false;
            case Context::user:
                if (unlikely(token.empty()))
                    throw Exception::UserFault{"Unexpected end-of-line while parsing a user name in the configuration file"};
                username = token;
                context = Context::pass;
                return true;
            case Context::pass:
                if (unlikely(token.empty()))
                    throw Exception::UserFault{"Unexpected end-of-line while parsing the password of user \"", username, "\" in the configuration file"};
                if (!users.emplace(::std::string{username}, ::std::string{token}).second) // Not inserted, username already exists
                    throw Exception::UserFault{"User \"", token, "\" specified more than once in the configuration file"};
                context = Context::none;
                return false;
            default: // Context::none
                if (token.empty()) { // New line: ignored
                    return false;
                } else if (token == "base") {
                    context = Context::base;
                    return true;
                } else if (token == "port") {
                    context = Context::port;
                    return true;
                } else if (token == "user") {
                    context = Context::user;
                    return false;
                } else {
                    throw Exception::UserFault{"Unexpected marker \"", token, "\" in the configuration file"};
                }
            }
        }
        /** Tokenize a memory buffer, ignore comments.
         * @param cursor First byte of the buffer
         * @param limit  First byte after the buffer
        **/
        void tokenize(char const* cursor, char const* limit) {
            constexpr static char linesep[]  = {'\n', '\r'};
            decltype(cursor) token = nullptr;
            bool new_line   = true;  // Whether the cursor is right after a new line
            bool till_break = false; // Whether the current token should continue until the next line break
            while (cursor < limit) { // Inter-line
                auto c = *(cursor++);
                auto is_linesep = ConstExpr::is_in(c, linesep);
                if (unlikely(c == '\0')) { // We don't want '\0' in a token...
                    throw Exception::UserFault{"Unexpected '\0' in configuration file"};
                } else if ((till_break && is_linesep) || (!till_break && c <= ' ')) { // Token seperator/blank space
                    if (token) {
                        till_break = digest(Token{token, static_cast<size_t>(cursor - 1 - token)});
                        if (is_linesep) // Digestion of an empty token
                            till_break = digest(Token{});
                        token = nullptr;
                    } else if (till_break) { // Empty token to generate
                        till_break = digest(Token{});
                    } // Else blank line or second char of Windows-style line-break: ignore
                    if (is_linesep)
                        new_line = true;
                } else if (new_line && c == '#') { // Comment line: ignore
                    while (cursor < limit) {
                        auto c = *(cursor++);
                        if (ConstExpr::is_in(c, linesep)) // Handle any end-line style
                            break;
                    }
                } else if (!token) { // Beginning of new token
                    token = cursor - 1;
                    new_line = false;
                } // Else go on with the current token
            }
            // Pending token
            if (token)
                digest(Token{token, static_cast<size_t>(limit - token)});
            // Safety checks
            switch (context) {
            case Context::base:
                throw Exception::UserFault{"Unexpected end-of-file while parsing 'base' in the configuration file"};
            case Context::port:
                throw Exception::UserFault{"Unexpected end-of-file while parsing 'port' in the configuration file"};
            case Context::user:
                throw Exception::UserFault{"Unexpected end-of-file while parsing a user name in the configuration file"};
            case Context::pass:
                throw Exception::UserFault{"Unexpected end-of-file while parsing a user password in the configuration file"};
            default: // Context::none
                break;
            }
            if (unlikely(users.size() == 0))
                throw Exception::UserFault{"No user specified in the configuration file"};
            if (unlikely(!service.size()))
                throw Exception::UserFault{"No port name/value specified in the configuration file"};
            if (unlikely(!custom_root))
                ::std::cout << "[UserFault] No base directory specified in the configuration file, falling back in the current directory" << ::std::endl;
        }
    };
    auto cursor = reinterpret_cast<char const*>(config.get_data());
    auto limit  = cursor + config.get_size();
    Parser{*this}.tokenize(cursor, limit);
}

// -------------------------------------------------------------------------- //
}
