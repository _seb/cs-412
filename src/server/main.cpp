/**
 * @file   main.cpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Server entry point.
**/

// External headers
#include <chrono>
#include <cstring>
#include <condition_variable>
#include <iostream>
#include <list>
#include <memory>
#include <mutex>
#include <random>
#include <thread>
#include <tuple>
#include <utility>
#include <vector>
extern "C" {
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
}

// Internal headers
#include <common.hpp>
#include <constexpr.hpp>
#include <optional.hpp>
#include <exception.hpp>
#include <filesystem.hpp>
#include <filequeue.hpp>
#include <network.hpp>
#include <open.hpp>
#include "config.hpp"

// -------------------------------------------------------------------------- //
// Fork, redirect and exec
namespace {

/** Fork, redirect stdout/stderr and execute a given program.
 * @param stdout Descriptor to use as 'stdout' and 'stderr'
 * @param file   Path to the program
 * @param wd     Working directory for the program
 * @param ...    Forwarded arguments
 * @return Error code, -1 for abnormal termination
 * @note No return for forked process
**/
template<class... Args> int exec(OS::Descriptor const& stdout, char const* file, FileSystem::Directory const& wd, Args&&... args) {
    // Pre-fork clone to handle close-on-exec file descriptor
    OS::Descriptor dup_stdout = OS::Descriptor{[&]() {
        auto fd = ::dup(stdout.get_fd());
        if (unlikely(fd == -1))
            throw Exception::External{"Duplicate fd ", stdout.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
        return fd;
    }()};
    OS::Descriptor dup_wd = OS::Descriptor{[&]() {
        auto fd = ::dup(wd.get_fd());
        if (unlikely(fd == -1))
            throw Exception::External{"Duplicate fd ", wd.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
        return fd;
    }()};
    // Process fork
    auto pid = ::fork();
    if (pid == 0) { // Child process
        if (unlikely(::fchdir(dup_wd.get_fd()) == -1))
            throw Exception::External{"Change directory fd ", dup_wd.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
        if (unlikely(::dup2(dup_stdout.get_fd(), STDOUT_FILENO) == -1))
            throw Exception::External{"Duplicate fd ", dup_stdout.get_fd(), " to stdout error code ", errno, " (", ::std::strerror(errno), ")"};
        if (unlikely(::dup2(dup_stdout.get_fd(), STDERR_FILENO) == -1))
            throw Exception::External{"Duplicate fd ", dup_stdout.get_fd(), " to stderr error code ", errno, " (", ::std::strerror(errno), ")"};
        ::std::string strv[] = {::std::string(args)...};
        char const* argv[sizeof...(args) + 2];
        for (size_t i = 0; i < sizeof...(args); ++i)
            argv[i + 1] = strv[i].data();
        argv[0] = file;
        argv[sizeof...(args) + 1] = nullptr;
        ::execvp(file, const_cast<char**>(argv)); // Abuse of const, historical
        throw Exception::External{"Execute \"", file, "\" error code ", errno, " (", ::std::strerror(errno), ")"};
    } else { // Parent process
        int status;
        if (unlikely(::waitpid(pid, &status, 0) < 0)) // Waiting failed
            throw Exception::External{"Wait pid ", pid, " error code ", errno, " (", ::std::strerror(errno), ")"};
        if (unlikely(!WIFEXITED(status))) // Not exited normally
            return -1;
        return WEXITSTATUS(status);
    }
}

}
// -------------------------------------------------------------------------- //
// Client context
namespace {

#define ERROR_HEAD  "Error: "
#define PATH_MAXLEN 128

/** Client connection, context and processing class.
**/
class ClientContext final: public Network::Socket {
private:
    /** Current client status class enum.
    **/
    enum class Status {
        anonymous, // Client is not authentified
        pending,   // Client is not authentified but has sent its username
        logged     // Client is authentified
    };
    /** Mutex class alias.
    **/
    using Mutex = ::std::mutex;
    /** List of logged-in client contexts class alias.
    **/
    using Logins = ::std::list<ClientContext const*>;
    /** Current registered login class alias.
    **/
    using Login = typename Logins::iterator;
private:
    Configuration::Configuration const* config; // Bound configuration instance
    Network::PeerName const peername; // Bound configuration instance
    Status                  status;   // Current client status (see 'Status')
    OS::FileMap             blob;     // Reception buffer (only growing) and pending command
    size_t                  size;     // Already received data size
    FileSystem::Directory   cwd;      // Current working directory
    FileQueue::Server&      fqueue;   // File queue at the server
private:
    static Mutex  list_lock;   // To serialize the access to member 'logins'
    static Logins list_logins; // Every logged-in client contexts
    Login         list_login;  // Iterator on this logged-in instance
    ::std::string username;    // Logged-in user name
private:
    /** Register the user in the logged-in user list.
    **/
    void login() {
        ::std::unique_lock<decltype(list_lock)> guard{list_lock};
        list_logins.push_front(this);
        list_login = list_logins.begin();
    }
    /** Unregister the user from the logged-in user list, no-op if not needed.
    **/
    void logout() noexcept {
        try {
            if (status == Status::logged) {
                ::std::unique_lock<decltype(list_lock)> guard{list_lock};
                list_logins.erase(list_login);
            }
        } catch (...) {
            ::std::cerr << "Uncaught exception in 'ClientContext::logout'" << ::std::endl;
        }
    }
public:
    /** Deleted copy constructor/assignment.
    **/
    ClientContext(ClientContext const&) = delete;
    ClientContext& operator=(ClientContext const&) = delete;
    /** Defaulted move constructor/assignment.
    **/
    ClientContext(ClientContext&&) = default;
    ClientContext& operator=(ClientContext&&) = default;
    /** Anonymous context constructor.
     * @param socket   Client socket to move
     * @param fqueue   Associated file queue to use
     * @param peername Client peer name
     * @param config   Configuration instance to bind
    **/
    ClientContext(Network::Socket&& socket, FileQueue::Server& fqueue, Network::PeerName&& peername, Configuration::Configuration const* config): Network::Socket{::std::move(socket)}, config{config}, peername{::std::move(peername)}, status{Status::anonymous}, blob(OS::sysconf(OS::SysConf::pagesize)), size{0}, cwd{}, fqueue{fqueue}, list_login{}, username{} {}
    /** Logout destructor.
    **/
    ~ClientContext() {
        logout();
    }
private:
    /** "Login" command processor.
     * @param params Additional parameters of the command
     * @return Whether this connection should be kept alive
    **/
    bool process_login(::std::string_view const& params) {
        if (status == Status::logged) {
            reply(ERROR_HEAD "already logged-in as user '", username, "'\n");
            return true;
        } // Allow successive 'login' commands (last one replace previous effects)
        username = ::std::string{params.data(), params.size()};
        status   = Status::pending;
        return true;
    }
    /** "Pass" command processor.
     * @param params Additional parameters of the command
     * @return Whether this connection should be kept alive
    **/
    bool process_pass(::std::string_view const& params) {
        if (status == Status::anonymous) {
            reply(ERROR_HEAD "please first indicate a username with the 'login' command\n");
            return true;
        } else if (status == Status::logged) {
            reply(ERROR_HEAD "please first logout with the 'logout' command\n");
            return true;
        }
        auto&& users = config->get_users();
        auto&& iter  = config->get_users().find(username);
        if (iter == users.cend() || iter->second != params) {
            // Add long (2 s on average), random sleep to make timing attacks extremely long to carry out
            std::random_device rd;
            std::uniform_int_distribution<uint_least16_t> dist{1000, 3000};
            ::std::this_thread::sleep_for(::std::chrono::milliseconds{dist(rd)});
            // Reply with no info on which (username or password) is incorrect
            reply(ERROR_HEAD "incorrect username/password\n");
            return true;
        }
        status = Status::logged;
        login();
        ::std::cerr << peername << ": logged in as '" << username << "'" << ::std::endl;
        return true;
    }
    /** "Ping" command processor.
     * @param params Additional parameters of the command
     * @return Whether this connection should be kept alive
    **/
    bool process_ping(::std::string_view const& params) {
        auto res = exec(*this, "ping", cwd, params, "-c", "1");
        if (res > 0) {
            reply(ERROR_HEAD "command returned error code ", ::std::to_string(res), "\n");
        } else if (res < 0) { // Abnormal termination
            reply(ERROR_HEAD "command terminated abnormally\n");
        }
        return true;
    }
    /** "List directory" command processor.
     * @param params <Ignored>
     * @return Whether this connection should be kept alive
    **/
    bool process_ls(::std::string_view const&) {
        auto res = exec(*this, "ls", cwd, "-l");
        if (res > 0) {
            reply(ERROR_HEAD "command returned error code ", ::std::to_string(res), "\n");
        } else if (res < 0) { // Abnormal termination
            reply(ERROR_HEAD "command terminated abnormally\n");
        }
        return true;
    }
    /** "Change directory" command processor.
     * @param params Additional parameters of the command
     * @return Whether this connection should be kept alive
    **/
    bool process_cd(::std::string_view const& params) {
        if (unlikely(params.size() > PATH_MAXLEN)) {
            reply(ERROR_HEAD "path too long\n");
            return true;
        }
        try {
            cwd = cwd.walk(params);
        } catch (Exception::FileSystem const&) {
            reply(ERROR_HEAD "unable to change directory\n");
        }
        return true;
    }
    /** "Make directory" command processor.
     * @param params Additional parameters of the command
     * @return Whether this connection should be kept alive
    **/
    bool process_mkdir(::std::string_view const& params) {
        if (unlikely(params.size() > PATH_MAXLEN)) {
            reply(ERROR_HEAD "path too long\n");
            return true;
        }
        try {
            cwd.mkdir(params);
        } catch (Exception::FileSystem const&) {
            reply(ERROR_HEAD "unable to make directory\n");
        }
        return true;
    }
    /** "Remove file/directory" command processor.
     * @param params Additional parameters of the command
     * @return Whether this connection should be kept alive
    **/
    bool process_rm(::std::string_view const& params) {
        if (unlikely(params.size() > PATH_MAXLEN)) {
            reply(ERROR_HEAD "path too long\n");
            return true;
        }
        try {
            cwd.rm(params);
        } catch (Exception::FileSystem const&) {
            reply(ERROR_HEAD "unable to remove directory\n");
        }
        return true;
    }
    /** "Get file" command processor.
     * @param params Additional parameters of the command
     * @return Whether this connection should be kept alive
    **/
    bool process_get(::std::string_view const& params) {
        // Assert path length
        if (unlikely(params.size() > PATH_MAXLEN)) {
            reply(ERROR_HEAD "path too long\n");
            return true;
        }
        // Open file for reading
        OS::Descriptor file;
        try {
            file = openfile(params, O_RDONLY, cwd);
        } catch (Exception::FileSystem const&) {
            reply("get port: ", fqueue.push(OS::Descriptor{}, 0, true), " size: 0\n"); // To keep sync
            reply(ERROR_HEAD "unable to open file for reading\n"); // For the user
            return true;
        }
        // Recover file size
        auto off = ::lseek(file.get_fd(), 0, SEEK_END);
        if (unlikely(off < 0 || ::lseek(file.get_fd(), 0, SEEK_SET) < 0)) {
            reply("get port: ", fqueue.push(OS::Descriptor{}, 0, true), " size: 0\n"); // To keep sync
            reply(ERROR_HEAD "unable to seek in opened file\n"); // For the user
            return true;
        }
        // Push file and size, reply with port and size
        reply("get port: ", fqueue.push(::std::move(file), off, true), " size: ", ::std::to_string(off), "\n");
        return true;
    }
    /** "Put file" command processor.
     * @param params Additional parameters of the command
     * @return Whether this connection should be kept alive
    **/
    bool process_put(::std::string_view const& params) {
        // Prepare input
        ::std::string_view path;
        size_t size = 0;
        { // Split 'file path' and 'size'
            char const* const data = params.data();
            size_t i = params.size();
            for (; i > 0; --i) { // Go to index past last space
                auto c = data[i - 1];
                if (c == ' ')
                    break;
                if (unlikely(c < '0' || c > '9')) {
                    reply(ERROR_HEAD "given size is not a valid non-negative integer\n"); // For the user (and assuming it was not a put command)
                    return true;
                }
            }
            if (unlikely(i == 0 || i == params.size())) { // Format error
                reply(ERROR_HEAD "expected format <file path> <size (in bytes)>\n"); // For the user (and assuming it was not a put command)
                return true;
            }
            { // Convert the size
                char const*      cursor = data + i;
                char const* const limit = data + params.size();
                while (cursor < limit) {
                    size = size * 10 + (*cursor - '0');
                    ++cursor;
                }
            }
            path = ::std::string_view{data, i - 1};
        }
        // Assert path length
        if (unlikely(path.size() > PATH_MAXLEN)) {
            reply(ERROR_HEAD "path too long\n");
            return true;
        }
        // Open file for writing
        OS::Descriptor file;
        try {
            file = openfile(path, O_WRONLY | O_CREAT | O_TRUNC, cwd);
        } catch (Exception::FileSystem const&) {
            reply("put port: ", fqueue.push(OS::Descriptor{}, 0, false), "\n"); // To keep sync
            reply(ERROR_HEAD "unable to open file for writing\n"); // For the user
            return true;
        }
        // Push file and size, reply with port
        reply("put port: ", fqueue.push(::std::move(file), size, false), "\n");
        return true;
    }
    /** "Grep" command processor.
     * @param params Additional parameters of the command
     * @return Whether this connection should be kept alive
    **/
    bool process_grep(::std::string_view const& params) {
        auto res = exec(*this, "grep", cwd, "-E", "-r", "-l", params);
        if (res > 0) {
            reply(ERROR_HEAD "command returned error code ", ::std::to_string(res), "\n");
        } else if (res < 0) { // Abnormal termination
            reply(ERROR_HEAD "command terminated abnormally\n");
        }
        return true;
    }
    /** "Date" command processor.
     * @param params <Ignored>
     * @return Whether this connection should be kept alive
    **/
    bool process_date(::std::string_view const&) {
        auto res = exec(*this, "date", cwd);
        if (res > 0) {
            reply(ERROR_HEAD "command returned error code ", ::std::to_string(res), "\n");
        } else if (res < 0) { // Abnormal termination
            reply(ERROR_HEAD "command terminated abnormally\n");
        }
        return true;
    }
    /** "Who am I" command processor.
     * @param params <Ignored>
     * @return Whether this connection should be kept alive
    **/
    bool process_whoami(::std::string_view const&) {
        reply(username, "\n");
        return true;
    }
    /** "W" command processor.
     * @param params <Ignored>
     * @return Whether this connection should be kept alive
    **/
    bool process_w(::std::string_view const&) {
        ::std::string text; // Reply text
        { // Build reply text
            ::std::unique_lock<decltype(list_lock)> guard{list_lock};
            for (auto&& context: list_logins) {
                if (text.size() > 0)
                    text += " ";
                text += context->username;
            }
        }
        reply(text, "\n");
        return true;
    }
    /** "Logout" command processor.
     * @param params <Ignored>
     * @return Whether this connection should be kept alive
    **/
    bool process_logout(::std::string_view const&) {
        logout();
        status = Status::anonymous;
        return true;
    }
    /** "Exit" command processor.
     * @param params <Ignored>
     * @return False because this connection should be shutdown
    **/
    bool process_exit(::std::string_view const&) {
        return false;
    }
private:
    /** Reply to the client with the given text.
     * @param ... Instances convertible to '::std::string_view' to reply with, in order
    **/
    template<class... Args> void reply(Args&&... args) {
        ::std::string_view text[] = {::std::forward<Args>(args)...};
        for (auto&& view: text)
            write(view.size(), view.data());
    }
    /** Process one command.
     * @param command String view on the command line
     * @return Whether this connection should be kept alive
    **/
    bool process(::std::string_view const& command) {
        ::std::string_view order;  // First continuous, UTF-8, lower-case word (because that is how the supported commands are)
        ::std::string_view params; // Remaining of the parameters, space-trimmed (i.e. ' ' only)
        { // Command split
            char const*      cursor = command.data();
            char const* const limit = cursor + command.size();
            // Order split
            while (cursor < limit && *cursor >= 'a' && *cursor <= 'z')
                ++cursor;
            order = ::std::string_view{command.data(), (cursor - command.data()) * sizeof(char)};
            // Space trim
            while (cursor < limit && *cursor == ' ')
                ++cursor;
            params = ::std::string_view{cursor, (limit - cursor) * sizeof(char)};
        }
        // Command processing
        constexpr static ::std::pair<char const*, bool (ClientContext::*)(std::string_view const&)> commands_anonymous[] = {
            {"login", &ClientContext::process_login},
            {"pass",  &ClientContext::process_pass},
            {"ping",  &ClientContext::process_ping},
            {"exit",  &ClientContext::process_exit}
        };
        constexpr static ::std::pair<char const*, bool (ClientContext::*)(std::string_view const&)> commands_loggedin[] = {
            {"ls",     &ClientContext::process_ls},
            {"cd",     &ClientContext::process_cd},
            {"mkdir",  &ClientContext::process_mkdir},
            {"rm",     &ClientContext::process_rm},
            {"get",    &ClientContext::process_get},
            {"put",    &ClientContext::process_put},
            {"grep",   &ClientContext::process_grep},
            {"date",   &ClientContext::process_date},
            {"whoami", &ClientContext::process_whoami},
            {"w",      &ClientContext::process_w},
            {"logout", &ClientContext::process_logout}
        };
        for (auto&& pair: commands_anonymous) {
            if (order == pair.first)
                return (this->*pair.second)(params);
        }
        if (status != Status::logged) {
            reply(ERROR_HEAD "unknown/unauthorized command: ", command, "\n");
            return true;
        }
        for (auto&& pair: commands_loggedin) {
            if (order == pair.first)
                return (this->*pair.second)(params);
        }
        reply(ERROR_HEAD "unknown command: ", command, "\n");
        return true;
    }
public:
    /** Read then process the received text.
     * @return Whether this connection should be kept alive
    **/
    bool process() {
        if (unlikely(size >= blob.get_size())) { // No space left
            reply(ERROR_HEAD "command too long\n");
            return false;
        }
        auto readsize = read(blob.get_size() - size, reinterpret_cast<char*>(blob.get_data()) + size);
        if (readsize > 0) {
            // Account for the read size
            size += readsize;
            // Process all found commands (1 command = 1 line terminated by '\n')
            char const* begin = nullptr; // Beginning of the current command (nullptr for none
            char const* cutto = nullptr; // Beginning of the partially received command
            char const* const limit = reinterpret_cast<char*>(blob.get_data()) + size;
            for (char* cursor = reinterpret_cast<char*>(blob.get_data()); cursor < limit; ++cursor) {
                unsigned char c = *cursor;
                if (c == '\n' || c == '\r') {
                    cutto = cursor + 1;
                    if (begin) { // Process command
                        if (!process(::std::string_view{begin, (cursor - begin) * sizeof(c)}))
                            return false;
                        begin = nullptr;
                    } // Else ignore
                } else if (c < 32 || c == 127) { // Invalid character (above is for UTF-8, let pass)
                    ::std::cout << peername << ": invalid input character" << ::std::endl;
                    return false;
                } else if (!begin) {
                    begin = cursor;
                }
            }
            // Move partially received command to the beginning of the blob
            if (cutto) { // Something to cut (at least one command processed)
                if (cutto < limit) { // Something to move
                    size = (limit - cutto) * sizeof(*cutto);
                    ::std::memmove(blob.get_data(), cutto, size);
                } else {
                    size = 0;
                }
            }
            // Continue processing commands
            return true;
        }
        return false;
    }
};

// Class-static variables
ClientContext::Mutex  ClientContext::list_lock;
ClientContext::Logins ClientContext::list_logins;

}
// -------------------------------------------------------------------------- //
// Basic management of connections
namespace {

/** Thread lifetime manager class.
**/
class ThreadSet final {
private:
    /** Mutex class alias.
    **/
    using Mutex = ::std::mutex;
    /** Condition variable class alias.
    **/
    using CondVar = ::std::condition_variable;
    /** One-shot event class alias.
    **/
    using Oneshot = OS::Oneshot;
private:
    Mutex   mutex;  // Lock for this structure
    CondVar zero;   // Notify the last thread at the time exited
    Oneshot stop;   // Stop request one-shot event
    size_t  count;  // Number of running threads
    bool   closing; // Instance is closing (purely informational)
    Oneshot closed; // Instance has been closed
public:
    /** Deleted copy/move constructor/assignment.
    **/
    ThreadSet(ThreadSet const&) = delete;
    ThreadSet& operator=(ThreadSet const&) = delete;
    /** Empty set constructor.
    **/
    ThreadSet(): mutex{}, zero{}, stop{}, count{0}, closing{false}, closed{} {}
    /** Flush pool destructor.
    **/
    ~ThreadSet() {
        try {
            flush();
            wait();
            closed.raise();
        } catch (::std::exception const& err) { // Do not forward the exception from a destructor, print error instead
            ::std::cerr << "Unable to wait for all the threads: " << err.what() << ::std::endl;
        }
    }
private:
    /** Increase the thread counter.
    **/
    void increase() {
        ::std::unique_lock<decltype(mutex)> lock{mutex};
        ++count;
    }
    /** Decrease the thread counter, notify when reaching 0.
    **/
    void decrease() {
        ::std::unique_lock<decltype(mutex)> lock{mutex};
        if (count == 0 || --count == 0) { // Handle fact that counter may have been reset to 0
            zero.notify_all();
            if (closing)
                closed.raise();
        }
    }
public:
    /** Get the closed event descriptor.
     * @return Closed event description
    **/
    Oneshot const& is_closed() const {
        return closed;
    }
public:
    /** Spawn another thread, would spawn a thread even if 'closing' is true (hence why 'closing' is said "informational").
     * @param socket Client socket to handle
     * @param entry  Entry point lambda, taking a reference to a one-shot event notifying stop request
    **/
    template<class Entry> void spawn(Entry&& entry) {
        increase();
        try {
            ::std::thread thread([this, entry = ::std::move(entry)]() mutable {
                try {
                    entry(const_cast<decltype(stop) const&>(stop));
                } catch (...) {
                    decrease();
                    throw;
                }
                decrease();
            }); // Safe to capture 'this' by reference, since ThreadSet::~ThreadSet ensures lifetime of 'this' surpasses lifetime of thread
            thread.detach(); // After this call, 'thread' can safely be destroyed
        } catch (...) {
            decrease();
        }
    }
    /** Raise the stop event.
    **/
    void flush() {
        stop.raise();
    }
    /** Mark the instance as closing, to get notified when reaching 0 workers.
    **/
    void mark_wait() {
        ::std::unique_lock<decltype(mutex)> lock{mutex};
        closing = true;
        if (count == 0) // Already empty
            closed.raise();
    }
    /** Wait for all the running threads, flush should have been called.
    **/
    void wait() {
        ::std::unique_lock<decltype(mutex)> lock{mutex};
        while (count > 0)
            zero.wait(lock);
    }
    /** Artificially reset the thread counter, effectively preventing waiting for them.
    **/
    void reset() {
        ::std::unique_lock<decltype(mutex)> lock{mutex};
        count = 0;
        zero.notify_all();
        closed.raise();
    }
};

}
// -------------------------------------------------------------------------- //
// Entry point

/** Function to call to show the flow is under the control of the adversary.
**/
void hijack_flow() {
    ::std::cout << "*** Flow hijacked. Congrats! ***" << ::std::endl;
}

/** Program entry point.
 * @param argc Arguments count
 * @param argv Arguments values
 * @return Program return code
**/
int main(int argc, char** argv) {
    try {
        OS::SignalContext sigcontext{OS::SignalAction::block, SIGINT, SIGTERM};
        if (argc != 2) {
            ::std::cout << "Usage: " << (argc > 0 ? argv[0] : "server") << " <server TCP service/port>" << ::std::endl;
            return 1;
        }
        Configuration::Configuration config{FileSystem::FileReadMap{"grass.conf"}};
        ThreadSet connections;
        // Server event multiplexing
        OS::Multiplex<void> multiplex;
        auto server = Network::Server{argv[1]};
        auto serobs = OS::Descriptor{server, OS::Descriptor::observe};
        multiplex.on(::std::move(server), [&](Network::Server& server, OS::AbstractMultiplex::Events events) {
            if (!events.has(EPOLLIN)) {
                ::std::cout << "Lost listening socket" << ::std::endl;
                multiplex.clear();
                return;
            }
            // Handling of one new connection
            connections.spawn([&, socket = server.accept()](OS::Oneshot const& stop) mutable {
                OS::SignalContext sigcontext{OS::SignalAction::block, SIGINT, SIGTERM};
                Network::PeerName peername;
                try {
                    peername = socket.peer_to_string();
                } catch (::std::exception const&) { // Arise when the client resets the connection right after connecting
                    return;
                }
                ::std::cout << peername << ": connected" << ::std::endl;
                { // Handler event multiplexing
                    OS::Multiplex<void> multiplex;
                    try {
                        FileQueue::Server fqueue{socket};
                        multiplex.on(OS::Descriptor{fqueue.get_fd(), OS::Descriptor::observe}, [&](OS::Descriptor&, OS::AbstractMultiplex::Events events) {
                            if (!events.has(EPOLLIN)) {
                                multiplex.clear();
                                return;
                            }
                            fqueue.pull();
                        });
                        multiplex.on(ClientContext{::std::move(socket), fqueue, ::std::string{peername}, &config}, [&](ClientContext& context, OS::AbstractMultiplex::Events events) {
                            if (!events.has(EPOLLIN) || !context.process())
                                multiplex.clear();
                        });
                        multiplex.on(OS::Descriptor{stop, OS::Descriptor::observe}, [&](OS::Descriptor&, OS::AbstractMultiplex::Events) {
                            multiplex.clear();
                        });
                        // Main handler loop
                        while (multiplex.pullable())
                            multiplex.pull();
                    } catch (::std::exception const& err) {
                        ::std::cerr << peername << ": " << err.what() << ::std::endl;
                        try {
#define ERROR_MSG ERROR_HEAD "internal server exception\n"
                            socket.write(sizeof(ERROR_MSG), ERROR_MSG);
#undef ERROR_MSG
                        } catch (::std::exception const& err) {
                            ::std::cerr << peername << ": " << err.what() << ::std::endl;
                        }
                    }
                }
                ::std::cout << peername << ": disconnected" << ::std::endl;
            });
        });
        multiplex.on(OS::SignalQueue{SIGINT, SIGTERM}, [&](OS::SignalQueue& queue, OS::AbstractMultiplex::Events) {
            queue.pop();
            ::std::cout << "\rWaiting for already connected clients..." << ::std::endl;
            multiplex.forget(serobs);
            connections.mark_wait();
            multiplex.on(::std::move(queue), [&](OS::SignalQueue& queue, OS::AbstractMultiplex::Events) {
                queue.pop();
                ::std::cout << "\rWaiting for current client operations only..." << ::std::endl;
                connections.flush();
                multiplex.on(::std::move(queue), [&](OS::SignalQueue& queue, OS::AbstractMultiplex::Events) {
                    queue.pop();
                    ::std::cout << "\rDropping connections and exiting now." << ::std::endl;
                    ::std::exit(0); // The only safe thing to do at this point
                });
            });
            multiplex.on(OS::Descriptor{connections.is_closed().get_fd(), OS::Descriptor::observe}, [&](OS::Descriptor&, OS::AbstractMultiplex::Events) {
                ::std::cout << "\rExiting now." << ::std::endl;
                multiplex.clear();
            });
        });
        // Main server loop
        while (multiplex.pullable())
            multiplex.pull();
        return 0;
    } catch (::std::exception const& err) {
        ::std::cerr << err.what() << ::std::endl;
        return 1;
    }
}
