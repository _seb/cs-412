/**
 * @file   filequeue.cpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Tailored file queue management.
**/

// External headers
#include <string>
#include <utility>
extern "C" {
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
}

// Internal headers
#include <common.hpp>
#include <constexpr.hpp>
#include <exception.hpp>
#include <filesystem.hpp>
#include <network.hpp>
#include <filequeue.hpp>

namespace FileQueue {
// -------------------------------------------------------------------------- //
// Helper functions
namespace {

/** Port number (network order) to string.
 * @param port Port number, throw if 0
 * @return Port string representation (UTF-8)
**/
inline auto port_to_string(uint16_t port) {
    if (unlikely(port == 0))
        throw Exception::FileQueue{"Null-port is reserved"};
    return ::std::to_string(ntohs(port));
}

/** Connect a TCP/IP socket for a different port from a given connected socket.
 * @param socket Given socket
 * @param port   Positive port number (host order)
 * @return New socket on the target port
**/
auto clone_connect(Network::Socket const& socket, uint16_t port) {
    port = htons(port);
    // Get the socket address
    ::sockaddr_storage addr;
    ::socklen_t len = sizeof(addr);
    if (unlikely(::getpeername(socket.get_fd(), reinterpret_cast<::sockaddr*>(&addr), &len) != 0))
        throw Exception::FileQueue{"Clone-connect socket ", socket.get_fd(), " peer name error code ", errno, " (", ::std::strerror(errno), ")"};
    // Change the target port
    switch (addr.ss_family) {
    case AF_INET: {
        auto& addr_ipv4 = reinterpret_cast<::sockaddr_in&>(addr);
        addr_ipv4.sin_port = port;
    } break;
    case AF_INET6: {
        auto& addr_ipv6 = reinterpret_cast<::sockaddr_in6&>(addr);
        addr_ipv6.sin6_port = port;
    } break;
    default:
        throw Exception::FileQueue{"Unsupported socket family ", addr.ss_family, " to clone-connect socket ", socket.get_fd()};
    }
    // Create the socket
    OS::Descriptor desc{::socket(addr.ss_family, SOCK_STREAM | SOCK_CLOEXEC, 0)};
    if (unlikely(!desc.is_valid_fd()))
        throw Exception::FileQueue{"Clone-connect ", socket.get_fd(), " socket error code ", errno, " (", ::std::strerror(errno), ")"};
    // Connect the socket
    if (unlikely(::connect(desc.get_fd(), reinterpret_cast<::sockaddr const*>(&addr), len) != 0))
        throw Exception::FileQueue{"Clone-connect ", socket.get_fd(), " connect ", desc.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
    // Return the socket
    return Network::Socket{::std::move(desc)};
}

/** Listen to TCP/IP connections for a different port from a given connected socket.
 * @param socket Given socket
 * @return Pair of (new listening socket on the target port/IP stack, bind port representation)
**/
auto clone_listen(Network::Socket const& socket) {
    // Get the connected socket family
    ::sockaddr_storage addr;
    ::socklen_t len = sizeof(addr);
    if (unlikely(::getsockname(socket.get_fd(), reinterpret_cast<::sockaddr*>(&addr), &len) != 0))
        throw Exception::FileQueue{"Clone-listen socket ", socket.get_fd(), " sock name error code ", errno, " (", ::std::strerror(errno), ")"};
    // Create the socket
    OS::Descriptor desc{::socket(addr.ss_family, SOCK_STREAM | SOCK_CLOEXEC, 0)};
    if (unlikely(!desc.is_valid_fd()))
        throw Exception::FileQueue{"Clone-listen ", socket.get_fd(), " socket error code ", errno, " (", ::std::strerror(errno), ")"};
    { // Replace socket in linger time
        int enable = 1;
        if (unlikely(::setsockopt(desc.get_fd(), SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1)) // Not crystal-clear 0 iff success, but -1 iff failure
            throw Exception::FileQueue{"Clone-listen socket ", desc.get_fd(), " reuse address error code ", errno, " (", ::std::strerror(errno), ")"};
    }
    // Listen to incoming connections (will bind to any free port)
    if (unlikely(::listen(desc.get_fd(), 1) != 0)) // Arbitrary backlog size
        throw Exception::FileQueue{"Clone-listen socket ", desc.get_fd(), " listen like ", socket.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
    // Get the socket bound port
    ::std::string port;
    if (unlikely(::getsockname(desc.get_fd(), reinterpret_cast<::sockaddr*>(&addr), &len) != 0))
        throw Exception::FileQueue{"Clone-listen socket ", desc.get_fd(), " sock name error code ", errno, " (", ::std::strerror(errno), ")"};
    switch (addr.ss_family) {
    case AF_INET:
        port = port_to_string(reinterpret_cast<::sockaddr_in&>(addr).sin_port);
        break;
    case AF_INET6:
        port = port_to_string(reinterpret_cast<::sockaddr_in6&>(addr).sin6_port);
        break;
    default:
        throw Exception::Unreachable{"Variable 'family' has unexpectedly been modified to an invalid value"};
    }
    // Return the socket
    return ::std::make_tuple<Network::Socket, ::std::string>(Network::Socket{::std::move(desc)}, ::std::move(port));
}

}
// -------------------------------------------------------------------------- //
// Server-side management

/** Clone constructor.
 * @param socket Client socket to clone
**/
Server::Server(Network::Socket const& socket): Multiplex<void>{}, port{}, files{} {
    auto tuple = clone_listen(socket);
    port = ::std::move(::std::get<1>(tuple));
    on(::std::move(::std::get<0>(tuple)), [&](Network::Socket& socket, OS::AbstractMultiplex::Events events) {
        // Handle error condition
        if (unlikely(!events.has(EPOLLIN)))
            throw Exception::FileQueue{"Queue lost listening socket ", socket.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
        // Accept the incoming connection
        OS::Descriptor desc{::accept(socket.get_fd(), nullptr, 0)};
        if (unlikely(!desc.is_valid_fd()))
            throw Exception::FileQueue{"Queue accept ", socket.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
        // Check if a file is indeed available (otherwise implies hijack/non-correct client)
        if (unlikely(files.size() == 0))
            throw Exception::FileQueue{"Possible data connection hijack (no file in queue)"};
        // Handle the client connection
        auto tuple = ::std::move(files.front());
        files.pop();
        if (unlikely(!::std::get<0>(tuple).is_valid_fd())) // Early close connection on invalid file
            return;
        if (::std::get<2>(tuple)) { // 'Get' case (server send data)
            on(Network::Socket{::std::move(desc)}, [this, file = ::std::move(::std::get<0>(tuple)), rem_size = ::std::get<1>(tuple), buffer = OS::FileMap(OS::sysconf(OS::SysConf::pagesize))](Network::Socket& socket, OS::AbstractMultiplex::Events events) mutable {
                try {
                    // Check if data remaining
                    if (rem_size == 0) {
                        forget(socket);
                        return;
                    }
                    // Check if can actually send data
                    if (!events.has(EPOLLOUT)) {
                        forget(socket);
                        return;
                    }
                    // Send as much data as possible
                    auto size = buffer.get_size();
                    if (size > rem_size)
                        size = rem_size;
                    rem_size -= size;
                    socket.write_all(file.read(size, buffer.get_data()), buffer.get_data());
                    // Non-termination check
                    if (rem_size > 0)
                        return;
                } catch (...) {
                    forget(socket);
                    throw;
                }
                forget(socket);
            }, EPOLLOUT);
        } else { // 'Put' case (server receive data)
            on(Network::Socket{::std::move(desc)}, [this, file = ::std::move(::std::get<0>(tuple)), rem_size = ::std::get<1>(tuple), buffer = OS::FileMap(OS::sysconf(OS::SysConf::pagesize))](Network::Socket& socket, OS::AbstractMultiplex::Events events) mutable {
                try {
                    // Check if data remaining
                    if (rem_size == 0) {
                        forget(socket);
                        return;
                    }
                    // Check if can actually receive data
                    if (!events.has(EPOLLIN)) {
                        forget(socket);
                        return;
                    }
                    // Receive as much data as possible
                    auto size = socket.read(buffer.get_size(), buffer.get_data());
                    if (size > rem_size)
                        size = rem_size;
                    rem_size -= size;
                    file.write_all(size, buffer.get_data());
                    // Non-termination check
                    if (rem_size > 0)
                        return;
                } catch (...) {
                    forget(socket);
                    throw;
                }
                forget(socket);
            }, EPOLLIN);
        }
    });
}

/** Push a file to transfer.
 * @param desc Valid descriptor of the file to transfer
 * @param size Size to transfer
 * @param send Whether to send the content (get) instead of receiving it (put)
 * @return Port name, with its lifetime bounded by the lifetime of '*this'
**/
::std::string_view Server::push(OS::Descriptor&& desc, size_t size, bool send) {
    files.emplace(::std::move(desc), size, send);
    return port;
}

// -------------------------------------------------------------------------- //
// Client-side management

/** Connect to a server file queue to receive a file ('get' case).
 * @param multiplex Multiplex to use
 * @param socket    Connection socket to mimic (i.e. use same network layer)
 * @param port      Positive port number to connect to (host order)
 * @param file      Open file for writing
 * @param size      Size to receive
**/
void recv(OS::Multiplex<bool>& multiplex, Network::Socket const& socket, uint16_t port, OS::Descriptor&& file, size_t size) {
    multiplex.on(clone_connect(socket, port), [&, file = ::std::move(file), rem_size = size, buffer = OS::FileMap(OS::sysconf(OS::SysConf::pagesize))](Network::Socket& socket, OS::AbstractMultiplex::Events events) mutable {
        try {
            // Check if data remaining
            if (rem_size == 0) {
                multiplex.forget(socket);
                return true;
            }
            // Check if can actually receive data
            if (!events.has(EPOLLIN)) {
                multiplex.forget(socket);
                throw Exception::FileQueue{"Data connection lost"};
            }
            // Receive as much data as possible
            auto size = socket.read(buffer.get_size(), buffer.get_data());
            if (size > rem_size)
                size = rem_size;
            rem_size -= size;
            file.write_all(size, buffer.get_data());
            // Non-termination check
            if (rem_size > 0)
                return true;
        } catch (...) {
            multiplex.forget(socket);
            throw;
        }
        multiplex.forget(socket);
        return true;
    }, EPOLLIN);
}

/** Connect to a server file queue to send a file ('put' case).
 * @param multiplex Multiplex to use
 * @param socket    Connection socket to mimic (i.e. use same network layer)
 * @param port      Positive port number to connect to (host order)
 * @param file      Open file for reading
 * @param size      Size to send
**/
void send(OS::Multiplex<bool>& multiplex, Network::Socket const& socket, uint16_t port, OS::Descriptor&& file, size_t size) {
    multiplex.on(clone_connect(socket, port), [&, file = ::std::move(file), rem_size = size, buffer = OS::FileMap(OS::sysconf(OS::SysConf::pagesize))](Network::Socket& socket, OS::AbstractMultiplex::Events events) mutable {
        try {
            // Check if data remaining
            if (rem_size == 0) {
                multiplex.forget(socket);
                return true;
            }
            // Check if can actually send data
            if (!events.has(EPOLLOUT)) {
                multiplex.forget(socket);
                throw Exception::FileQueue{"Data connection lost"};
            }
            // Send as much data as possible
            auto size = buffer.get_size();
            if (size > rem_size)
                size = rem_size;
            rem_size -= size;
            socket.write_all(file.read(size, buffer.get_data()), buffer.get_data());
            // Non-termination check
            if (rem_size > 0)
                return true;
        } catch (...) {
            multiplex.forget(socket);
            throw;
        }
        multiplex.forget(socket);
        return true;
    }, EPOLLOUT);
}

// -------------------------------------------------------------------------- //
}
