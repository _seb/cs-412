/**
 * @file   filesystem.cpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Low-level tailored file system management.
**/

// External headers
#include <cerrno>
#include <cstring>
#include <type_traits>
extern "C" {
#include <dirent.h>
#include <fcntl.h>
#include <grp.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>
}

// Internal headers
#include <common.hpp>
#include <constexpr.hpp>
#include <exception.hpp>
#include <filesystem.hpp>
#include <open.hpp>

namespace FileSystem {
// -------------------------------------------------------------------------- //
// Helper classes and functions
namespace {

/** Convert a protection to protection flags.
 * @param prot Given protection
 * @return Associated protection flags
**/
auto prot_to_flags(OS::FileMap::Protection prot) {
    auto val = static_cast<int>(prot);
    auto rd = (val & PROT_READ);
    auto wr = (val & PROT_WRITE);
    if (rd && wr) {
        return O_RDWR;
    } else if (rd) {
        return O_RDONLY;
    } else if (wr) {
        return O_WRONLY;
    } else {
        throw Exception::FileSystem{"Cannot open a file in execute-only mode"};
    }
}

/** Raw stat class alias.
**/
using RawStat = struct stat;

/** Whether to follow symlink enum class.
**/
enum class SymlinkFollow: bool {
    yes = true,
    no = false
};

/** Recover the stat of a given file.
 * @param stat Target raw stat buffer
 * @param path File path
 * @param base Base directory (optional, current directory by default)
 * @param fsym Whether to follow symlinks
**/
void stat(RawStat& stat, char const* path, SymlinkFollow fsym) {
    if ((static_cast<bool>(fsym) ? ::stat : ::lstat)(path, &stat) != 0)
        throw Exception::FileSystem{"Stat file \"", path, "\" error code ", errno, " (", ::std::strerror(errno), ")"};
}
void stat(RawStat& stat, char const* path, Directory const& base, SymlinkFollow fsym) {
    if (::fstatat(base.get_fd(), path, &stat, (static_cast<bool>(fsym) ? 0 : AT_SYMLINK_NOFOLLOW)) != 0)
        throw Exception::FileSystem{"Stat file \"", path, "\" from directory fd ", base.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
}

}
// -------------------------------------------------------------------------- //
// Directory walker

/** Current directory listing constructor.
**/
Directory::Directory(): Directory{"."} {}

/** Directory listing constructor.
 * @param path Path of the directory to list (null-terminated if raw pointer)
**/
Directory::Directory(char const* path): OS::Descriptor{[&]() {
    auto res = ::open(path, O_RDONLY | O_DIRECTORY | O_CLOEXEC);
    if (unlikely(res == -1))
        throw Exception::FileSystem{"Directory \"", path, "\" open error code ", errno, " (", ::std::strerror(errno), ")"};
    return res;
}()} {}
Directory::Directory(::std::string const& path): Directory{path.data()} {}

/** Open the directory following the given path from the current directory.
 * @param path Path to follow
 * @return Targeted directory
**/
Directory Directory::walk(char const* path) {
    auto res = ::openat(get_fd(), path, O_RDONLY | O_DIRECTORY | O_CLOEXEC);
    if (unlikely(res == -1))
        throw Exception::FileSystem{"Walk \"", path, "\" from ", get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
    return Directory{res};
}

/** Make a directory from the current directory.
 * @param path Path to make
**/
void Directory::mkdir(char const* path) {
    auto res = ::mkdirat(get_fd(), path, 0755); // Default mode with command 'mkdir'
    if (unlikely(res == -1))
        throw Exception::FileSystem{"Make directory \"", path, "\" from ", get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
}

/** Remove the file or the entire directory tree at the given path from the current directory.
 * @param path Path to remove
**/
void Directory::rm(char const* path) {
    RawStat res;
    stat(res, path, *this, SymlinkFollow::no);
    if (res.st_mode & S_IFDIR) { // Is directory
        Directory base = walk(path);
        DirectoryListing listing{base};
        for (auto&& tuple: listing.iterate<Generators::BelowOnly<Generators::Tuple<Generators::IsDir, Generators::Name>>>()) {
            auto&& name = ::std::get<1>(tuple);
            if (::std::get<0>(tuple)) { // Directory
                base.rm(name);
            } else { // Not directory
                if (unlikely(::unlinkat(base.get_fd(), name.data(), 0) == -1)) // String view is always null-terminated
                    throw Exception::FileSystem{"Remove file \"", name, "\" from ", get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
            }
        }
        if (unlikely(::unlinkat(get_fd(), path, AT_REMOVEDIR) == -1))
            throw Exception::FileSystem{"Remove directory \"", path, "\" from ", get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
    } else {
        if (unlikely(::unlinkat(get_fd(), path, 0) == -1))
            throw Exception::FileSystem{"Remove file \"", path, "\" from ", get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
    }
}

// -------------------------------------------------------------------------- //
// Directory walker

/** Entry as returned by 'getdents' class.
**/
class EntryRaw final {
public:
    ino64_t        ino;    // 64-bit inode number
    off64_t        off;    // 64-bit offset to next structure
    unsigned short reclen; // Size of this record
    unsigned char  type;   // File type
    char           name[]; // Filename (null-terminated)
};
static_assert(::std::is_standard_layout<EntryRaw>::value, "Expected 'EntryRaw' to be standard layout");

/** Current directory listing constructor.
**/
DirectoryListing::DirectoryListing(): DirectoryListing{"."} {}

/** DirectoryListing listing constructor.
 * @param path (Path of the) directory to list (null-terminated if raw pointer)
**/
DirectoryListing::DirectoryListing(Directory const& path): entries{4096 * 32} {
    { // Read the full raw entries
        size_t offset = reinterpret_cast<uintptr_t>(ConstExpr::align(reinterpret_cast<EntryRaw const*>(entries.get_data()))) - reinterpret_cast<uintptr_t>(entries.get_data()); // Offset in the filemap (to support moving remap), expected to always initialize to 0 (since entry alignment surely divides page alignment)
        while (true) {
            auto remain = (entries.get_size() - offset) / sizeof(EntryRaw);
            if (remain == 0) { // Not enough space for one entry
                entries.resize(entries.get_size() * 2); // Double the available space
                continue;
            }
            auto size = ::syscall(SYS_getdents64, path.get_fd(), ConstExpr::align(entries.get_data(), alignof(EntryRaw), offset), remain);
            if (unlikely(size < 0)) // Error
                throw Exception::FileSystem{"Directory fd ", path.get_fd(), " get entries error code ", errno, " (", ::std::strerror(errno), ")"};
            if (size == 0) // Done
                break;
            offset += size; // Empirically observed that 'size' is always a multiple of 'alignof(EntryRaw)'
        }
        entries.resize(offset);
    }
    { // Reorganize the raw entries into entries
        static_assert(sizeof(Entry) <= sizeof(EntryRaw) && alignof(Entry) <= alignof(EntryRaw), "Unable to process the raw entries inline");
        auto oldcur = ConstExpr::align(reinterpret_cast<EntryRaw const*>(entries.get_data()));
        auto limit  = ConstExpr::align(oldcur, alignof(EntryRaw), entries.get_size());
        auto newcur = ConstExpr::align(reinterpret_cast<Entry*>(entries.get_data()));
        while (oldcur < limit) {
            // Fully process old entry
            auto name = oldcur->name;
            auto size = ::std::strlen(name);
            FileType type;
            switch (oldcur->type) {
            case DT_BLK:
                type = FileType::blckdev;
                break;
            case DT_CHR:
                type = FileType::chardev;
                break;
            case DT_DIR:
                type = FileType::directory;
                break;
            case DT_FIFO:
                type = FileType::fifopipe;
                break;
            case DT_LNK:
                type = FileType::symlink;
                break;
            case DT_REG:
                type = FileType::regular;
                break;
            case DT_SOCK:
                type = FileType::socket;
                break;
            default:
                type = FileType::unknown;
                break;
            }
            oldcur = ConstExpr::align(oldcur, alignof(EntryRaw), oldcur->reclen);
            // Overwrite with new entry
            newcur->size = size;
            newcur->type = type;
            ::std::memmove(newcur->data, name, size + 1); // Including the null-terminator
            newcur = ConstExpr::align(newcur, alignof(Entry), sizeof(Entry) + size + 1);
        }
        entries.resize(reinterpret_cast<uintptr_t>(newcur) - reinterpret_cast<uintptr_t>(entries.get_data()), false, true); // Forced, final resize
    }
}
DirectoryListing::DirectoryListing(char const* path): DirectoryListing{Directory{path}} {}
DirectoryListing::DirectoryListing(::std::string const& path): DirectoryListing{Directory{path}} {}

// -------------------------------------------------------------------------- //
// Read-only file mapping

/** Current directory-relative constructor.
 * @param filepath Path to the file, relative to the current directory (if path is relative)
 * @param basedir  Base directory (optional, current directory by default)
**/
FileReadMap::FileReadMap(char const* filepath): OS::FileMap{[&]() -> size_t {
    RawStat buf;
    stat(buf, filepath, SymlinkFollow::yes);
    return buf.st_size;
}(), OS::FileMap::Protection::r__, openfile(filepath, prot_to_flags(OS::FileMap::Protection::r__))} {}
FileReadMap::FileReadMap(::std::string const& filepath): FileReadMap{filepath.data()} {}

// -------------------------------------------------------------------------- //
}
