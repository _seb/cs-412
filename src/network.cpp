/**
 * @file   network.cpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Low-level tailored network management.
**/

// External headers
#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <memory>
#include <string>
#include <type_traits>
#include <utility>
extern "C" {
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
}

// Internal headers
#include <common.hpp>
#include <exception.hpp>
#include <string_view.hpp>
#include <network.hpp>

namespace Network {
// -------------------------------------------------------------------------- //
namespace {

/** Generate the string representing the given address.
 * @param addr    Given address
 * @param addrlen Address length
 * @return String representation
**/
::std::string addrinfo_to_string(::sockaddr const* addr, ::socklen_t addrlen) {
    char host[NI_MAXHOST];
    char serv[NI_MAXSERV];
    auto code = getnameinfo(addr, addrlen, host, sizeof(host), serv, sizeof(serv), NI_NUMERICHOST | NI_NUMERICSERV);
    if (unlikely(code != 0))
        throw Exception::Network{"Address to string error code ", -code, " (", ::gai_strerror(code), ")"};
    ::std::string res;
    res.reserve(::std::string_view(host).size() + 1 + ::std::string_view(serv).size());
    res += host;
    res += ":";
    res += serv;
    return res;
}

/** Generate the string representing the given address.
 * @param info Given address info
 * @return String representation
**/
::std::string addrinfo_to_string(addrinfo const& info) {
    return addrinfo_to_string(info.ai_addr, info.ai_addrlen);
}

}
// -------------------------------------------------------------------------- //
// Helper classes

/** Free the associated resources, if any.
**/
void AddressInfo::cleanup() noexcept {
    if (info) {
        ::freeaddrinfo(info);
        info = nullptr;
    }
}

// -------------------------------------------------------------------------- //
// TCP-only socket mode-of-operation classes

/** Gather address information for connection purpose.
 * @param host Target host name/IP
 * @param serv Target service name/port
 * @return Raw, allocated address info
**/
AddressInfo Connect::make_info(char const* host, char const* serv) { // Make
    addrinfo hints;
    ::std::memset(&hints, 0, sizeof(addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    addrinfo* res;
    auto code = ::getaddrinfo(host, serv, &hints, &res);
    if (unlikely(code != 0))
        throw Exception::Network{"Address information for ", host, ":", serv, " error code ", -code, " (", ::gai_strerror(code), ")"};
    return AddressInfo{res};
}

/** Generate a connected socket to the current address.
 * @param info Raw address to connect to
 * @return Connected socket
**/
Socket Connect::make_socket(addrinfo const& info) {
    // Create the socket
    OS::Descriptor desc{::socket(info.ai_family, info.ai_socktype | SOCK_CLOEXEC, info.ai_protocol)};
    if (unlikely(!desc.is_valid_fd()))
        throw Exception::Network{"Connect ", addrinfo_to_string(info), " socket error code ", errno, " (", ::std::strerror(errno), ")"};
    // Connect the socket
    if (unlikely(::connect(desc.get_fd(), info.ai_addr, info.ai_addrlen) != 0))
        throw Exception::Network{"Socket ", desc.get_fd(), " connect to ", addrinfo_to_string(info), " error code ", errno, " (", ::std::strerror(errno), ")"};
    // Return the socket
    return Socket{::std::move(desc)};
}

/** Generate the string representing the given address.
 * @param info Given address
 * @return String representation
**/
::std::string Connect::make_string(addrinfo const& info) {
    return addrinfo_to_string(info);
}

/** Accept an incoming connection.
 * @return Incoming connection
**/
Socket Listen::ListenSocket::accept() {
    // Accept the socket
    OS::Descriptor desc{::accept(get_fd(), nullptr, 0)};
    if (unlikely(!desc.is_valid_fd()))
        throw Exception::Network{"Accept ", get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
    // Return the socket
    return Socket{::std::move(desc)};
}

/** Gather address information for listening purpose.
 * @param serv Local service name/port
 * @return Raw, allocated address info
**/
AddressInfo Listen::make_info(char const* serv) { // Make
    addrinfo hints;
    ::std::memset(&hints, 0, sizeof(addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    addrinfo* res;
    auto code = ::getaddrinfo(nullptr, serv, &hints, &res);
    if (unlikely(code != 0))
        throw Exception::Network{"Address information for ", serv, " error code ", -code, " (", ::gai_strerror(code), ")"};
    return AddressInfo{res};
}

/** Generate a listening socket to the current address.
 * @param info Raw address to listen to
 * @return Listening socket
**/
Listen::ListenSocket Listen::make_socket(addrinfo const& info) {
    // Create the socket
    OS::Descriptor desc{::socket(info.ai_family, info.ai_socktype | SOCK_CLOEXEC, info.ai_protocol)};
    if (unlikely(!desc.is_valid_fd()))
        throw Exception::Network{"Listen ", addrinfo_to_string(info), " socket error code ", errno, " (", ::std::strerror(errno), ")"};
    { // Replace socket in linger time
        int enable = 1;
        if (unlikely(::setsockopt(desc.get_fd(), SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1)) // Not crystal-clear 0 iff success, but -1 iff failure
            throw Exception::Network{"Socket ", desc.get_fd(), " reuse address error code ", errno, " (", ::std::strerror(errno), ")"};
    }
    // Bind the socket
    if (unlikely(::bind(desc.get_fd(), info.ai_addr, info.ai_addrlen) != 0))
        throw Exception::Network{"Socket ", desc.get_fd(), " bind to ", addrinfo_to_string(info), " error code ", errno, " (", ::std::strerror(errno), ")"};
    // Listen to incoming connections
    if (unlikely(::listen(desc.get_fd(), 4) != 0)) // Arbitrary backlog size
        throw Exception::Network{"Socket ", desc.get_fd(), " listen to ", addrinfo_to_string(info), " error code ", errno, " (", ::std::strerror(errno), ")"};
    // Return the socket
    return ListenSocket{::std::move(desc)};
}

/** Generate the string representing the given address.
 * @param info Given address
 * @return String representation
**/
::std::string Listen::make_string(addrinfo const& info) {
    return addrinfo_to_string(info);
}

// -------------------------------------------------------------------------- //
// Higher-level socket classes

/** Shutdown destructor.
**/
Socket::~Socket() {
    if (is_valid_fd() && !obs)
        ::shutdown(get_fd(), SHUT_RDWR);
}

/** Make the string representing the peer address.
 * @return Associated string representation
**/
PeerName Socket::peer_to_string() {
    ::sockaddr_storage addr;
    ::socklen_t len = sizeof(addr);
    if (unlikely(::getpeername(get_fd(), reinterpret_cast<::sockaddr*>(&addr), &len) != 0))
        throw Exception::Network{"Socket ", get_fd(), " peer name error code ", errno, " (", ::std::strerror(errno), ")"};
    return addrinfo_to_string(reinterpret_cast<::sockaddr const*>(&addr), len);
}

// -------------------------------------------------------------------------- //
}
