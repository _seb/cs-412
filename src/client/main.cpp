/**
 * @file   main.cpp
 * @author Sébastien Rouault <sebastien.rouault@epfl.ch>
 *
 * @section LICENSE
 *
 * Copyright © 2019 Sébastien ROUAULT.
 *
 * @section DESCRIPTION
 *
 * Client entry point.
**/

// External headers
#include <cstring>
#include <functional>
#include <iostream>
#include <queue>
#include <string>
#include <thread>
#include <tuple>
#include <vector>
extern "C" {
#include <fcntl.h>
#include <unistd.h>
}

// Internal headers
#include <common.hpp>
#include <constexpr.hpp>
#include <exception.hpp>
#include <filequeue.hpp>
#include <filesystem.hpp>
#include <network.hpp>
#include <open.hpp>
#include <string_view.hpp>

// -------------------------------------------------------------------------- //
// Simple line emitter

/** Simple line emitter class.
**/
class LineEmitter final {
private:
    OS::FileMap blob; // Received data
    size_t    offset; // Offset for the current line in the blob
    ::std::queue<::std::string> lines; // Not-yet-consumed lines
public:
    /** Empty constructor.
    **/
    LineEmitter() noexcept: blob(OS::sysconf(OS::SysConf::pagesize)), offset{0}, lines{} {}
public:
    /** Feed new data to the line emitter.
     * @param input Descriptor to read from
     * @return Whether the input provided data (if false, no new line)
    **/
    bool feed(OS::Descriptor& input) {
        // Reallocate (if need be)
        if (blob.get_size() == offset)
            blob.resize(2 * blob.get_size());
        // Read data
        auto cursor = reinterpret_cast<char*>(reinterpret_cast<uintptr_t>(blob.get_data()) + offset);
        auto size   = input.read(blob.get_size() - offset, cursor);
        if (unlikely(size == 0))
            return false;
        offset += size;
        char const* const limit = cursor + size;
        // Seek/emit new lines
        char const* start = reinterpret_cast<char const*>(blob.get_data());
        while (cursor < limit) {
            if (*cursor == '\n') {
                lines.emplace(start, cursor - start + 1); // Include the final '\n'
                start = cursor + 1; // Valid even if cursor is at the last character
            }
            ++cursor;
        }
        if (start != blob.get_data()) { // Need to move begun line
            offset = limit - start;
            ::std::memmove(blob.get_data(), start, offset);
        }
        return true;
    }
public:
    /** Check whether there are lines available.
     * @return Whether there are lines available
    **/
    bool has() const noexcept {
        return !lines.empty();
    }
    /** Pop (FIFO) one available line.
     * @return Popped line
    **/
    ::std::string pop() {
        ::std::string line = ::std::move(lines.front());
        lines.pop();
        return line;
    }
};

// -------------------------------------------------------------------------- //
// Entry point and argument parsing

/** Program entry point.
 * @param argc Arguments count
 * @param argv Arguments values
 * @return Program return code
**/
int main(int argc, char** argv) {
    ::std::thread infile_piper; // Pipe a regular file to be able to use 'epoll' consistently
    try {
        if (argc != 3 && argc != 5) {
            ::std::cout << "Usage: " << (argc > 0 ? argv[0] : "client") << " <server host/IP> <TCP service/port> [<infile> <outfile>]" << ::std::endl;
            return 1;
        }
        OS::SignalContext sigcontext{OS::SignalAction::block, SIGINT, SIGTERM};
        auto server = [&]() -> Network::Socket {
            Network::Addresses<Network::Connect> addresses{argv[1], argv[2]};
            for (auto& address: addresses) {
                try {
                    ::std::cout << "Connecting to " << address.to_string() << "..." << ::std::flush;
                    auto&& res = address.make_socket();
                    ::std::cout << " done." << ::std::endl;
                    return ::std::move(res);
                } catch (Exception::Network const& err) {
                    ::std::cout << " fail." << ::std::endl;
                }
            }
            throw Exception::Exception{"Unable to connect to the server."};
        }();
        Network::PeerName peername;
        try {
            peername = server.peer_to_string();
        } catch (::std::exception const&) {
            ::std::cout << "Server immediately dropped the connection" << ::std::endl;
        }
        OS::Descriptor infile{0, OS::Descriptor::observe};
        OS::Descriptor outfile{1, OS::Descriptor::observe};
        if (argc == 5) { // Replace input and output files in this case
            auto cmdfile = openfile(argv[3], O_RDONLY);
            OS::Descriptor pipe;
            { // Create pipe to 'infile'
                int fds[2];
                if (unlikely(::pipe2(fds, O_CLOEXEC) == -1))
                    throw Exception::External{"Pipe create error code ", errno, " (", ::std::strerror(errno), ")"};
                OS::Descriptor temp{fds[0]}; // Read end (to be duplicated then closed)
                pipe = OS::Descriptor{fds[1]}; // Write end
                if (unlikely(::dup3(temp.get_fd(), infile.get_fd(), O_CLOEXEC) == -1))
                    throw Exception::External{"Pipe ", temp.get_fd(), " duplicate on ", infile.get_fd(), " error code ", errno, " (", ::std::strerror(errno), ")"};
                infile = OS::Descriptor{infile.get_fd()}; // Remove observation
            }
            infile_piper = ::std::thread([&, cmdfile = ::std::move(cmdfile), blob = OS::FileMap(OS::sysconf(OS::SysConf::pagesize)), pipe = ::std::move(pipe)]() mutable {
                while (true) {
                    auto size = cmdfile.read(blob.get_size(), blob.get_data());
                    if (size == 0)
                        break;
                    pipe.write_all(size, blob.get_data());
                }
            });
            outfile = openfile(argv[4], O_WRONLY | O_CREAT | O_TRUNC); // Replace to output file
        }
        LineEmitter ingress;
        LineEmitter egress;
        ::std::queue<OS::Descriptor> gets; // Pending gets
        ::std::queue<::std::tuple<OS::Descriptor, size_t>> puts; // Pending puts
        OS::Multiplex<bool> multiplex;
        multiplex.on(::std::move(infile), [&](OS::Descriptor& input, OS::AbstractMultiplex::Events events) mutable {
            if (!events.has(EPOLLIN) || !ingress.feed(input)) {
                multiplex.forget(input);
                return true;
            }
            while (ingress.has()) {
                try {
                    auto line = ingress.pop();
                    [&]() -> void {
                        if (line.size() < 6) // Need at least 'xxx y\n'
                            return;
                        auto head = ::std::string_view{line.data(), 4}; // Select only 'xxx '
                        auto is_get = head == "get ";
                        auto is_put = head == "put ";
                        auto path = ::std::string_view{line.data() + 4, line.size() - 5}; // Select 'y...' without the '\n'
                        if (is_get) {
                            gets.emplace(openfile(path, O_WRONLY | O_CREAT | O_TRUNC));
                        } else if (is_put) {
                            size_t splitpos = path.size();
                            for (; splitpos > 0; --splitpos) {
                                if (path[splitpos] == ' ')
                                    break;
                            }
                            if (splitpos == 0)
                                return;
                            size_t size = 0;
                            for (size_t i = splitpos + 1; i < path.size(); ++i) {
                                auto c = path[i];
                                if (c < '0' || c > '9')
                                    throw Exception::UserFault{"Expected a non-negative integer for put size, got \"", ::std::string_view{path.data() + splitpos + 1, path.size() - splitpos - 1}, "\""};
                                auto nsize = 10 * size + (c - '0');
                                if (unlikely(nsize < size))
                                    throw Exception::UserFault{"Given non-negative integer for put size is too large"};
                                size = nsize;
                            }
                            puts.emplace(openfile(::std::string_view{path.data(), splitpos}, O_RDONLY), size);
                        }
                    }();
                    server.write_all(line.size(), line.data());
                } catch (::std::exception const& err) {
                    ::std::string_view msg{err.what()};
                    outfile.write_all(msg.size(), msg.data());
                    outfile.write_all(sizeof("\n"), "\n");
                }
            }
            return true;
        });
        multiplex.on(OS::Descriptor{server.get_fd(), OS::Descriptor::observe}, [&](OS::Descriptor&, OS::AbstractMultiplex::Events events) {
            if (!events.has(EPOLLIN) || !egress.feed(server)) {
                ::std::cout << "Disconnected from " << peername << " (server decision)" << ::std::endl;
                return false;
            }
            while (egress.has()) {
                auto line = egress.pop();
                if ([&]() -> bool {
                    // Basic analysis
                    if (line.size() < 12) // Need at least 'xxx port: y\n'
                        return true;
                    auto head = ::std::string_view{line.data(), 4}; // Select only 'xxx '
                    auto is_get = !gets.empty() && head == "get ";
                    auto is_put = !puts.empty() && head == "put ";
                    if (!is_get && !is_put)
                        return true;
                    // Tokenize
                    ::std::vector<::std::string_view> tokens;
                    {
                        char const*      cursor = line.data() + 4;
                        char const*       start = cursor;
                        char const* const limit = cursor + line.size() - 4; // With the final '\n'
                        while (cursor < limit) {
                            auto c = *cursor;
                            if (c == ' ' || c == '\n') {
                                tokens.emplace_back(start, cursor - start);
                                start = cursor + 1;
                            }
                            ++cursor;
                        }
                    }
                    // Check and parse port
                    if (tokens.size() < 2 || tokens[0] != "port:")
                        return true;
                    uint16_t port = 0;
                    { // Parse port
                        for (auto&& c: tokens[1]) {
                            if (c < '0' || c > '9')
                                return true;
                            auto nport = 10 * port + (c - '0');
                            if (nport < port) // Number to big for port number
                                return true;
                            port = nport;
                        }
                        if (port == 0) // Invalid port number
                            return true;
                    }
                    // Action-dependent processing
                    if (is_get) {
                        if (tokens.size() != 4 || tokens[2] != "size:")
                            return true;
                        size_t size = 0;
                        for (auto&& c: tokens[3]) {
                            if (c < '0' || c > '9')
                                return true;
                            auto nsize = 10 * size + (c - '0');
                            if (unlikely(nsize < size)) // Number to big for size
                                return true;
                            size = nsize;
                        }
                        auto file = ::std::move(gets.front());
                        gets.pop();
                        FileQueue::recv(multiplex, server, port, ::std::move(file), size);
                    } else { // 'is_put'
                        if (tokens.size() != 2)
                            return true;
                        auto pair = ::std::move(puts.front());
                        puts.pop();
                        FileQueue::send(multiplex, server, port, ::std::move(::std::get<0>(pair)), ::std::get<1>(pair));
                    }
                    // Do not print the received line
                    return false;
                }()) outfile.write_all(line.size(), line.data());
            }
            return true;
        });
        multiplex.on(OS::SignalQueue{SIGINT, SIGTERM}, [&](OS::SignalQueue& queue, OS::AbstractMultiplex::Events) {
            queue.pop();
            ::std::cout << "Disconnected from " << peername << ::std::endl;
            return false;
        });
        while (multiplex.pullable() && multiplex.pull());
        if (infile_piper.joinable())
            infile_piper.join();
        return 0;
    } catch (::std::exception const& err) {
        ::std::cerr << err.what() << ::std::endl;
        if (infile_piper.joinable())
            infile_piper.join();
        return 1;
    }
}
